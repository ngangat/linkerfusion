SRC=src
INC=inc
OBJ=obj
BIN=bin
EXE=fusion cat_elfheader cat_sectiontable cat_relocationtable cat_symboltable cat_readsect elf_fusion_1 elf_fusion_3
TESTDIR=tests

CC=gcc
CFLAGS=-Wall -Werror -g
LDFLAGS=-lm -lpthread -lgmp

RM=rm -fv

### Commandes pour l'indentation des sources #####
INDENT=indent
#ANSIFLG=-bap -bli0 -i2 -ncs -npcs -npsl -fca -fc1 -ts4
#INDNTFLG=-bad -bbb -bbo -cdb -lp -nbap -nut -sc -sob -ss -v -pmt

### Commandes pour la documentation ##############
GENDOC=doxygen
DOCCONFIG=./Doxyfile
DOCDIR=./docs/doxygen
##################################################

all: mkdirs $(EXE) clean-doc doc

fusion: $(OBJ)/fusion.o $(OBJ)/elf_header.o $(OBJ)/elf_sectiontable.o $(OBJ)/elf_readsect.o $(OBJ)/elf_symboltable.o $(OBJ)/elf_relocationtable.o
	$(CC) -o $(BIN)/$@ $^ $(LDFLAGS)
cat_elfheader: $(OBJ)/cat_elfheader.o $(OBJ)/elf_header.o
	$(CC) -o $(BIN)/$@ $^ $(LDFLAGS)
cat_sectiontable: $(OBJ)/cat_sectiontable.o $(OBJ)/elf_sectiontable.o $(OBJ)/elf_header.o $(OBJ)/elf_readsect.o
	$(CC) -o $(BIN)/$@ $^ $(LDFLAGS)
cat_symboltable: $(OBJ)/cat_symboltable.o $(OBJ)/elf_sectiontable.o $(OBJ)/elf_symboltable.o $(OBJ)/elf_header.o $(OBJ)/elf_readsect.o
	$(CC) -o $(BIN)/$@ $^ $(LDFLAGS)
cat_readsect: $(OBJ)/cat_readsect.o $(OBJ)/elf_sectiontable.o $(OBJ)/elf_readsect.o $(OBJ)/elf_header.o
	$(CC) -o $(BIN)/$@ $^ $(LDFLAGS)
cat_relocationtable: $(OBJ)/cat_relocationtable.o $(OBJ)/elf_sectiontable.o $(OBJ)/elf_header.o $(OBJ)/elf_relocationtable.o $(OBJ)/elf_readsect.o
	$(CC) -o $(BIN)/$@ $^ $(LDFLAGS)
elf_fusion_1: $(OBJ)/elf_fusion_1.o $(OBJ)/elf_sectiontable.o $(OBJ)/elf_header.o $(OBJ)/elf_fusionrenumsect.o $(OBJ)/elf_readsect.o
	$(CC) -o $(BIN)/$@ $^ $(LDFLAGS)
fusion_relocation: $(OBJ)/fusion_relocation.o $(OBJ)/elf_header.o $(OBJ)/elf_sectiontable.o $(OBJ)/elf_relocationtable.o $(OBJ)/elf_readsect.o
	$(CC) -o $(BIN)/$@ $^ $(LDFLAGS)
elf_fusion_3 : $(OBJ)/elf_fusion_3.o $(OBJ)/elf_header.o $(OBJ)/elf_sectiontable.o $(OBJ)/elf_fusionrenumsect.o $(OBJ)/elf_relocationtable.o $(OBJ)/elf_readsect.o
	$(CC) -o $(BIN)/$@ $^ $(LDFLAGS)

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) $(CFLAGS) -o $@  -c $< -I$(INC)

$(OBJ)/fusion.o: $(INC)/elf_header.h $(INC)/types.h $(INC)/elf_sectiontable.h $(INC)/elf_readsect.h $(INC)/elf_symboltable.h $(INC)/elf_relocationtable.h
$(OBJ)/cat_elfheader.o: $(INC)/elf_header.h $(INC)/types.h
$(OBJ)/elf_header.o: $(INC)/elf_header.h $(INC)/types.h
$(OBJ)/cat_sectiontable.o: $(INC)/types.h $(INC)/elf_header.h $(INC)/elf_sectiontable.h
$(OBJ)/elf_sectiontable.o: $(INC)/elf_sectiontable.h $(INC)/types.h $(INC)/elf_header.h $(INC)/elf_readsect.h
$(OBJ)/cat_relocationtable.o: $(INC)/types.h $(INC)/elf_header.h $(INC)/elf_sectiontable.h $(INC)/elf_relocationtable.h
$(OBJ)/elf_relocationtable.o: $(INC)/elf_relocationtable.h $(INC)/elf_sectiontable.h $(INC)/types.h $(INC)/elf_header.h
$(OBJ)/cat_readsect.o: $(INC)/types.h $(INC)/elf_header.h $(INC)/elf_sectiontable.h $(INC)/elf_readsect.h
$(OBJ)/elf_readsect.o: $(INC)/elf_readsect.h $(INC)/elf_header.h $(INC)/types.h 
$(OBJ)/elf_symboltable.o: $(INC)/elf_symboltable.h $(INC)/types.h $(INC)/elf_header.h $(INC)/elf_readsect.h
$(OBJ)/cat_symboltable.o: $(INC)/types.h $(INC)/elf_header.h $(INC)/elf_sectiontable.h $(INC)/elf_symboltable.h $(INC)/elf_readsect.h
$(OBJ)/elf_fusionrenumsect.o: $(INC)/types.h $(INC)/elf_header.h $(INC)/elf_sectiontable.h $(INC)/elf_fusionrenumsect.h
$(OBJ)/fusion_relocation.o: $(INC)/types.h $(INC)/elf_header.h $(INC)/elf_sectiontable.h $(INC)/elf_readsect.h $(INC)/elf_relocationtable.h
$(OBJ)/elf_fusion_3.o: $(INC)/types.h $(INC)/elf_header.h $(INC)/elf_sectiontable.h $(INC)/elf_readsect.h $(INC)/elf_fusionrenumsect.h $(INC)/elf_relocationtable.h

run-tests: $(EXE)
	for i in $^; do ./$(BIN)/$$i $(TESTDIR)/$(wildcard *.o); done

doc:
	$(GENDOC) $(DOCCONFIG)

.PHONY: clean mrproper mkdirs clean-doc

clean:
	$(RM) $(OBJ)/*.o $(SRC)/*~ $(INC)/*~ $(SRC)/*.orig $(INC)/*.orig
clean-doc:
	$(RM) -r $(DOCDIR)/*
mrproper: clean
	$(RM) $(BIN)/*
mkdirs:
	mkdir -p  $(SRC) $(INC) $(BIN) $(OBJ) $(DOCS) $(TESTDIR)

$(INDENT): $(SRC)/*.c $(INC)/*.h
	astyle -A10 -a -f -c -k3 -Z -z2 -M79 -R "./src/*.c" "./inc/*.h"
#$(INDENT) $(ANSIFLG) $(INDNTFLG) $^

