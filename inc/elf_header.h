#ifndef ELF_HEADER_H
#define ELF_HEADER_H

#include <stdio.h>
#include <stdlib.h>
#include <elf.h>
#include "types.h"

/**
 * @brief lit l'entête du fichier objet et remplit une structure header associée
 * @param file fichier objet Elf32 à lire
 * @param header retour: adresse d'un pointeur sur un header d'Elf32
 * @return 0 si il n'y a pas d'erreur
 */
int Elf32_read_header(FILE *file, Elf32_Ehdr **header);

/**
 * @brief vérifie que les 3 premiers octets d'un fichier correspondent au format ELF
 * @param file pointeur sur le fihier à lire
 * @return 1 si le magic number est conforme au format ELF, 0 sinon
 */
int Elf32_verify_magic(FILE *file);

/**
 * @brief affiche le header d'un fichier objet valide Elf32
 * @param header pointeur sur header de fichier objet préalablement lu
 */
void Elf32_print_header(Elf32_Ehdr *header);

#endif
