#ifndef ELF_PROGBITS_H
#define ELF_PROGBITS_H

#include <stdlib.h>
#include <stdio.h>
#include <elf.h>
#include "types.h"
#include "elf_header.h"

/* Concatène deux section PROGBITS dans la première et modifie le header
 * section1* et sect1 sont paramètre resultat
 * retourne l'offset de la concaténation
 * Effet de bord : alloue de la mémoire (sect)
 */
/**
 * @brief
 *
 * @param section1
 * @param sect1
 * @param section2
 * @param sect2
 *
 * @return
 */
int concatsection(Elf32_Shdr *section1, char **sect1, Elf32_Shdr *section2, char **sect2);

/* cherche la table des string a partir de la table des sections et le header
 * param : table des sections, header ELF, fichier, resultat table des strings
 * retourne un éventuelle code d'erreur
 * lis dans le fichier, alloue de la mémoire
 */
/**
 * @brief
 *
 * @param file
 * @param sect_table
 * @param header
 * @param stringtable
 *
 * @return
 */
int stringtable_section_name(FILE *file, Elf32_Shdr **sect_table, Elf32_Ehdr *header, char **stringtable);

/* cherche une section de même nom dans une table des symboles
 * param : table des sections, table de string correspondante, nom a rechercher
 * retourne l'indice de la section de même nom, sinon retourne -1
 * va etre modifier
 */
/**
 * @brief
 *
 * @param section2
 * @param shtable1
 * @param file1
 * @param header1
 *
 * @return
 */
int find(Elf32_Shdr *section2, Elf32_Shdr **shtable1, FILE *file1, Elf32_Ehdr *header1);

/**
 * @brief
 *
 * @param sect_table
 * @param string_table
 * @param name
 * @param nb_sh
 *
 * @return
 */
int findsection(Elf32_Shdr **sect_table, char **string_table, char *name,int nb_sh);

/* ajoute une section dans un fichier
 * non prioritaire
 */
/**
 * @brief
 *
 * @param section1
 * @param sect1
 * @param result
 *
 * @return
 */
int writesection(Elf32_Shdr *section1, char **sect1, FILE *result);

/* ajoute un header dans la table des sections
 * retourne la nouvelle position du header
 * Effet de bord : alloue de la mémoire
 */
/**
 * @brief
 *
 * @param section1
 * @param shtable
 * @param nb
 *
 * @return
 */
int addheader(Elf32_Shdr *section1, Elf32_Shdr **shtable, int nb);

/* concataine deux headers avec PROGBITS en un seul header dans une table des symboles
 * le header est paramètre resultat
 * retourne un code d'erreur
 * pas d'effet de bord
 */
/**
 * @brief
 *
 * @param section1
 * @param section2
 *
 * @return
 */
int concatheader(Elf32_Shdr *section1, Elf32_Shdr *section2);

/*ecrit la table des sections*/

#endif
