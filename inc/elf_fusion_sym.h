#ifndef ELF_FUSION_SYM_H
#define ELF_FUSION_SYM_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <elf.h>

#include "types.h"
#include "elf_header.h"
#include "elf_symboltable.h"

/**
 * @brief fusionne deux tables des strings
 * @param head1 pointeur sur section header fichier 1
 * @param head2 pointeur sur section header fichier 2
 * @param table1 tableau de la table des strings du fichier 1
 * @param table2 tableau de la table des strings du fichier 2
 * @return offset de la table des strings ou erreur
 */
int fusion_stringtable(Elf32_Shdr *head1, char **table1, Elf32_Shdr *head2, char **table2);

/**
 * @brief met a jour les adresses des noms
 * @param offset offset des names dans la table des strings
 * @param symtable2 retour: table des symboles
 * @param tailletable taille de la table
 * @return 0 ou erreur
 */
int fusion_modifname(int offset, Elf32_Sym **symtable2, int tailletable);

/**
 * @brief met à jour les adresses des symboles dans les fonctions
 * @param symtable2
 * @param tailletable
 * @param offset
 * @return
 */
int fusion_modif_offset(Elf32_Sym **symtable2, int tailletable, int *offset);

/**
 * @brief met à jours les adresses des symboles dans les fonctions
 * @param symtable2
 * @param tailletable
 * @param indice
 * @return
 */
int fusion_maj_shndx(Elf32_Sym **symtable2, int tailletable, int *indice);

/**
 * @brief fusionne les tables des symboles
 * @param symtable1
 * @param symtable2
 * @param head1
 * @param head2
 * @param table1
 * @param taille1
 * @param taille2
 * @return
 */
int fusionner_table(Elf32_Sym **symtable1, Elf32_Sym **symtable2, Elf32_Shdr *head1, Elf32_Shdr *head2, char **table1, int taille1, int taille2);


#endif
