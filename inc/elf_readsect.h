#ifndef ELF_READSECT_H
#define ELF_READSECT_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <elf.h>
#include "elf_header.h"


/**
 * @brief cherche le contenu d'une section
 * @param file fichier objet Elf32
 * @param header pointeur sur le section header à lire
 * @param sect retour: adresse d'un tableau d'octets représentant le contenu de la section
 * @return retourne la taille de la section lue
 */
int Elf32_read_sect(FILE *file, Elf32_Shdr *header, char **sect);

/**
 * @brief affiche le contenu d'une section
 * @param file fichier objet Elf32
 * @param header pointeur sur un section header
 */
void Elf32_printf_sect(FILE *file, Elf32_Shdr *header);

#endif
