#ifndef _FUSIONRENUMSECT_H_
#define _FUSIONRENUMSECT_H_

#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "elf_sectiontable.h"
#include "elf_readsect.h"

/**
 * @brief concatène une section du fichier obj1 avec une section du fichier obj2
 * @param obj1 fichier objet 1
 * @param obj2 fichier objet 2
 * @param sh_obj1 pointeur sur un header de section se trouvant dans le fichier 1
 * @param sh_obj2 pointeur sur un header de section se trouvant dans le fichier 2
 * @return octets de la section 1 concaténée à la suite de la section 2
 */
char *Elf32_shconcat(FILE *obj1, Elf32_Shdr *sh_obj1, FILE *obj2, Elf32_Shdr *sh_obj2);

/**
 * @brief fusionne les sections de type SHT_PROGBITS de deux fichiers objet
 * @param obj1 fichier objet 1
 * @param obj2 fichier objet 2
 * @param header1 pointeur sur le header du fichier obj1
 * @param header2 pointeur sur le header du fichier obj2
 * @param sh_table1 tableau des headers des sections du fichier obj1
 * @param sh_table2 tableau des headers des sections du fichier obj2
 * @param sh_table1_names tablea des noms des sections du fichier obj1
 * @param sh_table2_names tablea des noms des sections du fichier obj2
 * @param sh_table_res retour: pointeur sur un tableau des headers de sections gardés après traitement
 * @param assoc_name retour: tableau des noms de header de section correspondant à sh_table_res
 * @param table_section retour: pointeur sur un tableau de sections éventuellement concaténées
 * @param section_offset retour: pointeur sur un tableau d'offsets correspondants au décalage permettant d'atteindre une section concaténée à une autre à partir de l'offset de cette dernière
 * @param sh_table1_ndx retour: pointeur sur un tableau permettant de retrouver  l'index d'un header de section à partir de son index dans le fichier obj1
 * @param sh_table2_ndx retour: pointeur sur un tableau permettant de retrouver  l'index d'un header de section à partir de son index dans le fichier obj2
 * @return nombre d'éléments dans le tableau sh_table_res
 */
int Elf32_fusion_progbits(FILE *obj1, Elf32_Ehdr *header1, Elf32_Shdr *sh_table1, char **sh_table1_names
                          , FILE *obj2, Elf32_Ehdr *header2, Elf32_Shdr *sh_table2, char **sh_table2_names
                          , Elf32_Shdr **sh_table_res, char *** assoc_names, char *** table_section, int **section_offset
                          , int **sh_table1_ndx, int **sh_table2_ndx
                         );
#endif

