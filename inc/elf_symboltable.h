#ifndef ELF_SYMBOLETABLE_H
#define ELF_SYMBOLETABLE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <elf.h>
#include <string.h>


#include "elf_header.h"
#include "elf_readsect.h"
#define ERRCODE_EOF -1 ;
#define ERRCODE_MALLOC -2 ;

/**
 * @brief lit une entrée de la table des symboles
 * @param file fichier objet Elf32
 * @return entrée lue
 */
Elf32_Sym Elf32_read_Esym(FILE *file);

/**
 * @brief lit l'ensemble de la table des symboles
 * @param file fichier objet Elf32
 * @param sect section header de la table des symboles
 * @param symtb retour: adresse d'un tableau de symboles
 * @return 0 ou code d'erreur
 */
int Elf32_read_symtb(FILE *file, Elf32_Shdr *sect, Elf32_Sym **symtb);

/**
 * @brief affiche une ligne de la table des symboles
 * @param sym symbole à afficher
 * @param sh_strtab tableau des noms des symboles
 */
void Elf32_print_sym(Elf32_Sym *sym, char **sh_strtab);

/**
 * @brief affiche l'ensemble de la table des symboles
 * @param simtb tableau des symboles
 * @param symtab_hdr section header associé à la table des symboles
 * @param nb nombre d'entrées dans la table des symboles
 * @param str_tab tableau des noms des symboles
 */
void Elf32_print_symtb(Elf32_Sym *simtb, Elf32_Shdr *symtab_hdr,int nb, char **str_tab);   // printf

/**
 * @brief lis la table des string correspondant à la table des symboles
 * @param file fichier objet Elf32
 * @param sh_symtb section header associé à la table des symboles
 * @param sh_table adresse du tableau des section headers
 * @param str_tab tableau des noms des symboles
 * @return 0 ou code d'erreur
 */
int Elf32_read_symtb_strtb(FILE *file, Elf32_Shdr *sh_symtb, Elf32_Shdr **sh_table, char **str_tab);

/**
 * @brief retourne le nom d'un symbole dans la table des strings à partir d'un index
 * @param str_tab table des strings
 * @param index index du nom à retourner
 * @return nom correspondant à l'index donnée
 */
char *get_name(char *str_tab, int index);
#endif
