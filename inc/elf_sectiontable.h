#ifndef ELF_SECTION_TABLE_H
#define ELF_SECTION_TABLE_H

#include <stdlib.h>
#include <stdio.h>
#include <elf.h>
#include <string.h>
#include "types.h"
#include "elf_header.h"
#include "elf_readsect.h"

/**
 * @brief lit les headers de sections dans un fichier objet Elf32
 * @param file fichier objet Elf32
 * @param header header du fichier objet Elf32
 * @param sh_table retour: adresse d'un tableau de headers de sections
 * @return nombre d'éléments lus
 */
int Elf32_read_sectiontable(FILE *file, Elf32_Ehdr *header,
                            Elf32_Shdr **sh_table);

/**
 * @brief afficher un header de section
 * @param sh_table pointeur sur un header de section
 * @return 0
 */
int Elf32_print_sheader(Elf32_Shdr *sh_table);

/**
 * @brief retourne l'index du header de section correspondant à la table des symboles
 * @param header pointeur vers le header du fichier obj
 * @param sheader tableau des section headers
 * @return index ou code d'erreur
 */
int Elf32_seek_indexsymtab(Elf32_Ehdr *header, Elf32_Shdr *sheader);

/**
 * @brief rempli un tableau de headers de sections de type SHT_REL à partir d'un tableau de section headers
 * @param sh_table tableau des section headers
 * @param rel_table retour: adresse d'un tableau de section headers de type SHT_REL
 * @return nombre d'éléments dans le tableau rempli
 */
int Elf32_seek_reltable(Elf32_Shdr *sh_table, Elf32_Shdr **rel_table);

/**
 * @brief retourne le section header associé à la table des str
 * @param header pointeur sur un header Elf32 de fichier objet
 * @param sh_table tableau des section headers du fichier objet
 * @return pointeur sur le section header de type SHT_STRTAB du fichier objet
 */
Elf32_Shdr *Elf32_seek_shstr(Elf32_Ehdr *header, Elf32_Shdr *sh_table);

/* retourne les nom d'une section
 * paramètre :le fichier, le header de la tbale des strings,
 * le header de la section que l'on veux interpreter
 * retourne le nom de la section
 * lis dans le fichier
 */
/**
 * @brief retourne le nom d'un section header
 * @param file fichier objet Elf32
 * @param shstr pointeur sur le section header de type SHT_STRTAB du fichier objet
 * @param sheader pointeur sur le section header à traiter
 * @return chaine de caractères correspondant au nom de la section header donnée
 */
char *Elf32_seek_nameshstr(FILE *file, Elf32_Shdr *shstr, Elf32_Shdr *sheader);

#endif
