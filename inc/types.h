#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>

/* types */
typedef uint32_t Elf32_Addr;
typedef uint32_t Elf32_Off;
typedef uint16_t Elf32_Half;
typedef int32_t Elf32_Sword;
typedef uint32_t Elf32_Word;

/* codes d'erreur */
#define ERRCODE_BADALLOC -1     /* allocation échouée */
#define ERRCODE_BADMOVE -2      /* echec lors de déplacement dans le fichier*/
#define ERRCODE_NOENTRY -3
#define ERRCODE_READFILE -4     /* echec lors de la lecture dans le fichier */
#define ERRCODE_SHNULL -5
#define ERRCODE_NOFILE -6
#define ERRCODE_BADFILE -7

#endif
