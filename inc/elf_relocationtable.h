#ifndef ELF_RELOCATION_TABLE_H
#define ELF_RELOCATION_TABLE_H

#include <stdlib.h>
#include <stdio.h>
#include <elf.h>
#include "types.h"
#include "elf_header.h"
#include "elf_sectiontable.h"

/**
 * @brief lit une table de relocation associée à un section header
 * @param header pointeur sur un header de fichier obj
 * @param sh_table tableau des section headers du fichier obj
 * @param sh_reltable retour adresse d'un pointeur sur une section de type SHT_REL
 * @return 0 ou erreur
 */
int Elf32_read_relocationtable(Elf32_Ehdr *header,
                               Elf32_Shdr *sh_table,
                               Elf32_Shdr **sh_reltable);

/**
 * @brief trouve les sections de type SHT_REL à partir d'un section header de type SHT_REL
 * @param file fichier objet Elf32
 * @param sh_relheader pointeur sur un section header de type SHT_REL
 * @param rel retour: adresse d'un tableau de sections SHT_REL associés au section header donné en paramètre
 * @return nombre d'éléments dans le tableau
 */
int Elf32_seek_rel(FILE *file, Elf32_Shdr *sh_relheader, Elf32_Rel **rel);

/**
 * @brief retourne une chaine correspondant au type d'une entrée d'une section de relocation
 * @param relocation type de l'entrée (nombre)
 * @return chaine de caractères correspondant au type
 */
char *rtype_str(unsigned int type);
#endif
