#!/bin/bash  

PREFIX=`fgrep "setenv ARM_PREFIX" setenvarm.csh | sed -e "s/setenv ARM_PREFIX *//"`

function delay1 () {
  echo -n .; sleep 1
}

function delay4 () {
  delay1; delay1; delay1; delay1 
  echo; echo; echo
}

function check_gcc {
  if ! [ -f ${PREFIX}gcc ] 
  then
    echo WARNING : ${ARM_PREFIX}gcc missing
    delay4
  fi
}

function check_gdb {
  if ! [ -f ${PREFIX}gdb ]
  then
    echo WARNING : ${PREFIX}gdb missing
    delay4
  fi
  }
  
function check_ddd () {
  DDD=`which ddd | grep -v "not found" |wc -l`
  if [ $DDD -eq 0 ] 
  then
    echo WARNING : ddd command not found
    echo Is the ddd package installed ?
    echo Did you add the ddd binary directory in default PATH ?
    delay4
  fi
}

check_gcc
check_gdb
check_ddd

   
