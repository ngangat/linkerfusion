# setenv for Bourne shell : will source setenvarm.csh

function setenv () {
   local var=$1
   shift
   eval "$var='$*'"
   export $var
}

function alias () {
  local eval_this var
  case "$#" in 
     0) # alias without arguments 
        builtin alias
     ;;
     1) # alias for Bourne syntax : single arguments = string "cde=definition"
        builtin alias "$1"
     ;;
     *) # alias for csh syntax  with >= 2 arguments 
        var=$1
        shift
        builtin alias $var="$*"
     ;;
  esac
}

ENVDIR=/home/ngangat/Documents/L3-info/PROG5/linkerFusion/envarm
source ${ENVDIR}/setenvarm.csh
