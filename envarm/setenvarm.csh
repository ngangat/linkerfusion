# Base Directory
setenv ARM_ENVDIR /home/ngangat/Documents/L3-info/PROG5/linkerFusion/envarm

# Les executables de compilation
setenv ARM_PREFIX /opt/gnu/arm/bin/arm-elf-
setenv ARM_GDBDIR /opt/gnu/arm/bin
#echo
setenv SIM_GCC "${ARM_PREFIX}gcc -Wall -gdwarf-2"
setenv ARM_AS "${ARM_PREFIX}as --gdwarf2"
setenv SIM_LD ${ARM_PREFIX}ld
setenv ARM_READELF ${ARM_PREFIX}readelf
setenv ARM_OBJDUMP ${ARM_PREFIX}objdump
setenv ARM_AR ${ARM_PREFIX}ar
setenv ARM_STRIP ${ARM_PREFIX}strip
setenv ARM_RUN_COMMAND ${ARM_PREFIX}run
# exec-ed by xxxgdb script
setenv ARM_GDB_COMMAND ${ARM_GDBDIR}/arm-elf-gdb

# Path : include ARM_ENVDIR
setenv PATH ${PATH}:${ARM_ENVDIR}

alias simgcc ${SIM_GCC}
alias armgcc ${SIM_GCC}
alias armas ${ARM_AS}
alias armreadelf ${ARM_READELF}
alias armobjdump ${ARM_OBJDUMP}
alias armar ${ARM_AR}
alias simgdb ${ARM_ENVDIR}/simgdb
alias armgdb ${ARM_ENVDIR}/simgdb
alias armrun ${ARM_RUN_COMMAND}
alias simddd ${ARM_ENVDIR}/simddd
alias armddd ${ARM_ENVDIR}/simddd

echo
echo "Environment variables defined :"
env | grep "^ARM"
env | grep "^SIM"
echo 
echo "New alias commands :"
alias | sed -e "s/^alias[ 	]*//" | grep "^arm" 
echo
