#!/bin/bash 

# check file exists  #
if ! [ -f $1 ] 
then
  echo "ERROR while checking file exists"
  echo $1 "   --> file missing or special file type (directory, pipe, ...)"
  exit 1
fi

# check file is rx #
if ! [ -r $1 ] || ! [ -x $1 ]
then
  echo "ERROR while checking file rights"
  echo $1 "  --> not a readable and executable file"
  exit 1
fi

# check file is ELF #
is_elf=`file $1 | grep ELF | wc -l`
if [ ${is_elf} == 0 ] 
then
  echo "ERROR while checking file content is elf"
  echo $1 " --> not an ELF file"
  exit 1
fi

# check file is executable ELF #
is_exec=`${ARM_READELF} -h $1 | grep Type | grep EXEC | wc -l`
if [ $is_exec == 0 ]
then
  echo "ERROR while checking ELF file is executable"
  echo $1 "  --> not an executable ELF file"
  exit 1
fi

# file is ARM executable #
processor=`${ARM_READELF} -h $1 | grep "Machine"`
is_arm=`echo $processor | grep ARM | wc -l`
processor=`echo $processor | sed -e "s/.*Machine:[ 	]*//"`

if [ $is_arm != 1 ]
then
  echo "ERROR while checking executable file processor"
  echo $1 " --> compiled for $processor instead of ARM"
  echo      --> Use armgcc or instead of gcc
  exit 1
fi
exit 0;
