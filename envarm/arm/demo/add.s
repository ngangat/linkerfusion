@ ------------------------------------------
@ MIAGE2  - Module ALM -TD application
@ add.s
@ ------------------------------------------

     .text
     .global main

main:
      MOV    r0, #0
      
      MOV    r1, #3
      ADD    r1, r0, r1, LSL #28
      MOV    r2, #4
      ADD    r2, r0, r2, LSL #28
      ADDS   r3, r1, r2
obs1:
      MOV    r1, #4
      ADD    r1, r0, r1, LSL #28
      MOV    r2, #5
      ADD    r2, r0, r2, LSL #28
      ADDS   r3, r1, r2
obs2:
      MOV    r1, #5
      ADD    r1, r0, r1, LSL #28
      MOV    r2, #14
      ADD    r2, r0, r2,LSL #28
      ADDS   r3, r1, r2
obs3:
      MOV    r1, #11
      ADD    r1, r0, r1, LSL #28
      MOV    r2, #4
      ADD    r2, r0, r2, LSL #28
      ADDS   r3, r1, r2
obs4:
      MOV    r1, #15
      ADD    r1, r0, r1, LSL #28
      MOV    r2, #1
      ADD    r2, r0, r2, LSL #28
      ADDS   r3, r1, r2
obs5:
      MOV    pc, lr

@ OBSERVATION DES VALEURS DES CODES CONDITIONS APRES UNE ADDITION
@ ================================================================
@ apr�s avoir r�cuper� ce fichier dans add.s ex�cuter la commande :
@ arm-elf-gcc -Wa,--gdwarf2,-L -o add add.s
@ Lancer le simulateur ddd avec la commande :
@ ddd  -debugger arm-elf-gdb
@
@ On obtient une grande fenetre avec une partie dite "source" (en haut)
@ que l'on n'utilisera pas et une partie dite "console" (en bas) ou l'on
@ entrera certaines commandes. 
@
@ Dans la fen�tre "console" taper successivement les commandes :
@ file add
@         on voit appara�tre le source du programme en langage
@         d'assemblage dans la fen�tre "source" et une petite fen�tre de
@         commandes
@ target sim 
@ load add
@
@ Maintenant on peut commencer la simulation.
@
@ ----------------------------------------------------------------------
@ Placer un point d'arr�t sur la premi�re instruction :
@    s�lectionner la ligne en question avec la souris et cliquer sur
@    l'ic�ne break (dans le panneau sup�rieur)
@
@ Cliquer sur le bouton Run (fen�tre "commandes") pour lancer
@ l'ex�cution.
@
@    On peut relancer le programme autant de fois que l'on veut
@    en cliquant sur le bouton Run.
@
@ Vous voyez apparaitre une fl�che verte qui vous indique la position du
@ compteur programme i.e. ou vous en �tes de l'ex�cution de votre
@ programme
@
@ Le bouton Step permet l'ex�cution d'une ligne de code.
@ Le bouton Cont permet de poursuivre l'execution.
@    on peut par exemple placer un point d'arr�t sur une ligne
@    quelconque du programme. Cont permet de poursuivre l'ex�cution
@    jusqu'� ce nouveau point d'arr�t.
@ ----------------------------------------------------------------------
@ Pour enlever un point d'arret :
@     delete break numero_du_point_d'arret dans la fenetre "console"
@     ou
@     se positionner sur la ligne desiree et cliquer � nouveau sur
@     l'ic�ne break
@     bouton droit de la souris, selectionner Clear at.
@
@ Pour voir le contenu des registres
@     menu Status : Registers -> une fenetre apparait
@     La valeur contenue dans chaque registre est donnee en hexadecimal
@     (0x...) et en decimal.
@     Remarquer le registre cpsr (registre d'etat) dans lequel on trouve
@     entre autres les valeurs des indicateurs arithmetiques
@     (Z, N, C et V) en position 31,30,29,28 (c.a.d. � l'extr�me gauche
@     du registre...).
@
@ Pour initialiser un registre
@     dans la fenetre "console" : set $r5=3
@
@ Lorsque l'on ne s'int�resse aux valeurs que de quelques registres, on
@ peut les faire afficher dans une fen�tre "data". Pour la cr�er
@ utiliser le menu View, puis Data Window.
@ Apr�s avoir fait afficher la fen�tre des registres cliquer (1 fois)
@ sur le registre qui vous int�resse, et utiliser l'ic�ne Display (dans
@ le panneau superieur) : en gardant le bouton de gauche de la souris
@ appuye, choisir le format d'affichage.
@ Pour afficher un seul registre, on peut aussi utiliser la commande :
@ graph display nom_du_registre (par exemple $r3) dans la fenetre
@ "console"
@
@ ==============  TRAVAIL PRATIQUE =====================================
@ mettre un point d'arr�t sur obs1, obs2, obs3, obs4, obs5
@ lancer l'ex�cution, le programme est stopp� � chaque point d'arr�t
@ noter alors la valeur des codes de conditions arithm�tiques C et Z.
@
@ vous pouvez aussi ex�cuter le programme en mode pas � pas sans mettre
@ de point d'arr�t et regarder les valeur de C et Z en obs1, obs2, etc.
@
@ donner une interpr�tation des valeurs des codes de conditions
@ arithm�tiques selon les valeurs contenues dans les registres lors de
@ chaque addition
@ ======================================================================
