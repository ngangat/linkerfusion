/***********************************************************************
 * samsung.h
 *
 * Auteur : Pierre Habraken
 *
 * Registres du micro-controleur Samsung KS32C50100
 **********************************************************************/

#ifndef SAMSUNG_H
#define SAMSUNG_H

/* Timers */

#define TMOD      (unsigned int *)0x3ff6000 // Timer mode register
#define TE0       (1 << 0)    // Timer 0 enable
#define TMD0      (1 << 1)    // Timer 0 mode selection
#define TCLR0     (1 << 2)    // Timer 0 initial TOUT0 value
#define TE1       (1 << 3)    // Timer 1 enable
#define TMD1      (1 << 4)    // Timer 1 mode selection
#define TCLR1     (1 << 5)    // Timer 1 initial TOUT0 value

#define TDATA0    (unsigned int *)0x3ff6004 // Timer 0 data register
#define TDATA1    (unsigned int *)0x3ff6008 // Timer 1 data register

#define TCNT0     (unsigned int *)0x3ff600c // Timer 0 count register
#define TCNT1     (unsigned int *)0x3ff6010 // Timer 1 count register


/* Ports d'entree-sortie */

#define IOPMOD    (unsigned int *)0x3ff5000 // I/O port mode register

#define IOPDATA   (unsigned int *)0x3ff5008 // I/O port data register

#define IOPCON    (unsigned int *)0x3ff5004 // I/O port control register

#define CONxIRQ0  0           // External interrupt request 0 (port 8)
#define CONxIRQ1  5           // External interrupt request 1 (port 9)
#define CONxIRQ2  10          // External interrupt request 2 (port 10)
#define CONxIRQ3  15          // External interrupt request 3 (port 11)
#define CONDRQ0   20          // External DMA request 0 (port 12)
#define CONDRQ1   23          // External DMA request 1 (port 13)
#define CONDAK0   26          // External DMA acknowledge 0 (port 14)
#define CONDAK1   28          // External DMA acknowledge 1 (port 15)
#define CONTOEN0  30          // Timeout 0 (port 16)
#define CONTOEN1  31          // Timeout 1 (port 17)

#define IRQ_ACTIVE  (1<<4)      /* activer fonction IRQ de la broche */
#define IRQ_HIGH    (1<<3)      /* si niveau : IRQ si signal == 1    */
#define IRQ_FILTER  (1<<2)      /* activation antirebond             */
#define IRQ_LEVEL   0           /* IRQ sur signal == niveau          */
#define IRQ_REDGE   1           /* IRQ front montant                 */
#define IRQ_FEDGE   2           /* IRQ front descendant              */
#define IRQ_BEDGE   3           /* IRQ fronts montant et descendant  */

#define DRQ_ACTIVE  (1 << 2)     /* activer fonction DRQ de la broche  */
#define DRQ_FILTER  (1 << 1)     /* activation antirebond             */
#define DRQ_HIGH    (1 << 0)     /* DRQ si signal == 1                */

#define DAK_ACTIVE  (1 << 2)     /* activer contion DAK de la broche  */
#define DAK_FILTER  (1 << 1)     /* activation antirebond             */
#define DAK_HIGH    (1 << 0)     /* DAK si signal == 1                */

#define TOEN_ACTIVE (1 << 0)     /* activation sortie temporisateur   */

#define IRQ0_ACTIVE (IRQ_ACTIVE << CONxIRQ0)
#define IRQ1_ACTIVE (IRQ_ACTIVE << CONxIRQ1)
#define IRQ2_ACTIVE (IRQ_ACTIVE << CONxIRQ2)
#define IRQ3_ACTIVE (IRQ_ACTIVE << CONxIRQ3)

#define DRQ0_ACTIVE (DRQ_ACTIVE << CONDRQ0)
#define DRQ1_ACTIVE (DRQ_ACTIVE << CONDRQ1)
#define DAK0_ACTIVE (DAK_ACTIVE << CONDAK0)
#define DAK1_ACTIVE (DAK_ACTIVE << CONDAK1)

#define TOEN0_ACTIVE (TOEN_ACTIVE << CONTOEN0)
#define TOEN1_ACTIVE (TOEN_ACTIVE << CONTOEN1)

/* Controleur d'interruptions */

#define INTMOD    (unsigned int *)0x3ff4000 // Interrupt mode register
#define NUMxIRQ0  0           // External interrupt 0
#define NUMxIRQ1  1           // External interrupt 1
#define NUMxIRQ2  2           // External interrupt 2
#define NUMxIRQ3  3           // External interrupt 3
#define NUMUART0X 4           // UART0 transmit interrupt
#define NUMUART0R 5           // UART0 receive and error interrupt
#define NUMUART1X 6           // UART1 transmit interrupt
#define NUMUART1R 7           // UART1 receive and error interrupt
#define NUMGDMA0  8           // GDMA channel 0 interrupt
#define NUMGDMA1  9           // GDMA channel 1 interrupt
#define NUMTIMER0 10          // Timer 0 interrupt
#define NUMTIMER1 11          // Timer 1 interrupt
#define NUMHDLCAX 12          // HDLC channel A Tx interrupt
#define NUMHDLCAR 13          // HDLC channel A Rx interrupt
#define NUMHDLCBX 14          // HDLC channel B Tx interrupt
#define NUMHDLCBR 15          // HDLC channel B Rx interrupt
#define NUMETHBDX 16          // Ethernet controller BDMA Tx interrupt
#define NUMETHBDR 17          // Ethernet controller BDMA Rx interrupt
#define NUMETHMCX 18          // Ethernet controller MAC Tx interrupt
#define NUMETHMCR 19          // Ethernet controller MAC Rx interrupt
#define NUMI2C    20          // I2C interrupt
#define INTxIRQ0  (1 << NUMxIRQ0)
#define INTxIRQ1  (1 << NUMxIRQ1)
#define INTxIRQ2  (1 << NUMxIRQ2)
#define INTxIRQ3  (1 << NUMxIRQ3)
#define INTUART0X (1 << NUMUART0X)
#define INTUART0R (1 << NUMUART0R)
#define INTUART1X (1 << NUMUART1X)
#define INTUART1R (1 << NUMUART1R)
#define INTGDMA0  (1 << NUMGDMA0)
#define INTGDMA1  (1 << NUMGDMA1)
#define INTTIMER0 (1 << NUMTIMER0)
#define INTTIMER1 (1 << NUMTIMER1)
#define INTHDLCAX (1 << NUMHDLCAX)
#define INTHDLCAR (1 << NUMHDLCAR)
#define INTHDLCBX (1 << NUMHDLCBX)
#define INTHDLCBR (1 << NUMHDLCBR)
#define INTETHBDX (1 << NUMETHBDX)
#define INTETHBDR (1 << NUMETHBDR)
#define INTETHMCX (1 << NUMETHMCX)
#define INTETHMCR (1 << NUMETHMCR)
#define INTI2C    (1 << NUMI2C)

#define INTPND    (unsigned int *)0x3ff4004 // Interrupt pending register

#define INTMSK    (unsigned int *)0x3ff4008 // Interrupt mask register
#define GBLINTMSK (1 << 21)   // Global interrupt mask bit

#define INTPRI0   (unsigned int *)0x3ff400C // Int. priority register 0
#define NUMPRI0   0
#define NUMPRI1   8
#define NUMPRI2   16
#define NUMPRI3   24

#define INTPRI1   (unsigned int *)0x3ff4010 // Int. priority register 1
#define NUMPRI4   0
#define NUMPRI5   8
#define NUMPRI6   16
#define NUMPRI7   24

#define INTPRI2   (unsigned int *)0x3ff4014 // Int. priority register 2
#define NUMPRI8   0
#define NUMPRI9   8
#define NUMPRI10  16
#define NUMPRI11  24

#define INTPRI3   (unsigned int *)0x3ff4018 // Int. priority register 3
#define NUMPRI12  0
#define NUMPRI13  8
#define NUMPRI14  16
#define NUMPRI15  24

#define INTPRI4   (unsigned int *)0x3ff401C // Int. priority register 4
#define NUMPRI16  0
#define NUMPRI17  8
#define NUMPRI18  16
#define NUMPRI19  24

#define INTPRI5   (unsigned int *)0x3ff4020 // Int. priority register 5
#define NUMPRI20  0

#define INTOFFSET (unsigned int *)0x3ff4024 // Interrupt offset register
#define INTOSET_FIQ (unsigned int *)0x3ff4030 // FIQ Int. offset register
#define INTOSET_IRQ (unsigned int *)0x3ff4034 // IRQ Int. offset register

#endif
