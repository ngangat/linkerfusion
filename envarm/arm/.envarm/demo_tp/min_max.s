@   Expansion :
@   
@   if (* (unsigned int *)&x >= * (unsigned int *)&y) goto	sinon;
@     * (unsigned int *)&max = * (unsigned int *)&y;
@     * (unsigned int *)&min = * (unsigned int *)&x;
@     goto fin_si;
@   sinon:
@     * (unsigned int *)&max = * (unsigned int *)&x;
@     * (unsigned int *)&min = * (unsigned int *)&y;
@   fin_si:
@     somme = * (unsigned int *)&x + * (unsigned int *)&y;
@   
@   
@   ! &x,x : r0   &y,y : r1    &min : r2    &max : r3    somme : r4
@   
	.data
min:	.skip	4
max:	.skip	4
x:	.word	78
y:	.word	53

	.text
	.global	main
main:	ldr     r0, =x
	ldr	r0,[r0]
	ldr     r1, =y
	ldr	r1,[r1]
	cmp     r0, r1
	bhs	sinon		@ bge en l'absence de unsigned
alors:  ldr	r2, =min
	ldr     r3, =max
	str	r1, [r3]	@ r1 contient deja *&y
	str	r0, [r2]        @ r0 contient deja *&x
	b	fin_si; nop
sinon:	ldr     r2, =min
	ldr     r3, =max
	str	r0, [r3]
	str	r1, [r2]
fin_si:	add	r4, r0, r1
fin:	b	fin;nop		@ on fait boucler (ne sachant pas terminer)
	.ltorg


