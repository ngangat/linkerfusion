# pour (t)csh
#
# Utilisation : source setenvarm.csh
#
setenv GNUARMDIR /opt/gnu/arm
setenv PATH ~/.envarm:${GNUARMDIR}/bin:$PATH
if ($?MANPATH) then
  setenv MANPATH ${GNUARMDIR}/man:$MANPATH
else setenv MANPATH ${GNUARMDIR}/man
endif
setenv DDD_STATE ~/.envarm
alias armddd ddd --debugger ~/.envarm/armgdb
alias carteddd ddd --debugger ~/.envarm/cartegdb
alias armgcc arm-elf-gcc -g -Wa,-L
# Ajouter ici l'option bigendian
#
alias armbiggcc arm-elf-gcc -mbig-endian -g -Wa,-L
alias armld arm-elf-ld
alias armbigld arm-elf-ld -EB
alias armas arm-elf-as
alias armbigas arm-elf-as -EB
#
echo "Voici la liste des pseudo-commandes (alias) pour arm"
alias | grep  armddd  
alias | grep  carteddd  
alias | grep  armgcc  
alias | grep  armbiggcc  
alias | grep  armld  
alias | grep  armbigld  
alias | grep  armas  
alias | grep  armbigas  
