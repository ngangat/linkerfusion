#include "samsung.h"
#include "evaluator.h"

/*****************************************************************************/
/* ports.c                                                                   */
/*                                                                           */
/* Auteur : Philippe Waille                                                  */
/* a partir des fichiers samsung.h et evaluator.h                            */
/*                                                                           */
/* noms symboliques des registres du micro-controleur Samsung KS32C50100     */
/*                                                                           */
/* pour affichage sous gdb/dd                                                */
/*                                                                           */
/* ***************************************************************************/

char dispo_iopdata [] = {"---------------fgedcba-vLLLiiii"};
const unsigned int * cTMOD = TMOD;
const int cTE0 = TE0;
const int cTMD0 = TMD0;
const int cTCLR0 = TCLR0;
const int cTE1 = TE1;
const int cTMD1 = TMD1;
const int cTCLR1 = TCLR1;

const unsigned int * cTDATA0 = TDATA0;
const unsigned int * cTDATA1 = TDATA1;

const unsigned int * cTCNT0 = TCNT0;
const unsigned int * cTCNT1 = TCNT1;



const unsigned int * cIOPMOD = IOPMOD;

const unsigned int * cIOPCON = IOPCON;
const int cCONxIRQ0 = CONxIRQ0;
const int cCONxIRQ1 = CONxIRQ1;
const int cCONxIRQ2 = CONxIRQ2;
const int cCONxIRQ3 = CONxIRQ3;
const int cCONDRQ0 = CONDRQ0;
const int cCONDRQ1 = CONDRQ1;
const int cCONDAK0 = CONDAK0;
const int cCONDAK1 = CONDAK1;
const int cCONTOEN0 = CONTOEN0;
const int cCONTOEN1 = CONTOEN1;

const unsigned int * cIOPDATA = IOPDATA;



const unsigned int * cINTMOD = INTMOD;
const int cNUMxIRQ0 = NUMxIRQ0;
const int cNUMxIRQ1 = NUMxIRQ1;
const int cNUMxIRQ2 = NUMxIRQ2;
const int cNUMxIRQ3 = NUMxIRQ3;
const int cNUMUART0X = NUMUART0X;
const int cNUMUART0R = NUMUART0R;
const int cNUMUART1X = NUMUART1X;
const int cNUMUART1R = NUMUART1R;
const int cNUMGDMA0 = NUMGDMA0;
const int cNUMGDMA1 = NUMGDMA1;
const int cNUMTIMER0 = NUMTIMER0;
const int cNUMTIMER1 = NUMTIMER1;
const int cNUMHDLCAX = NUMHDLCAX;
const int cNUMHDLCAR = NUMHDLCAR;
const int cNUMHDLCBX = NUMHDLCBX;
const int cNUMHDLCBR = NUMHDLCBR;
const int cNUMETHBDX = NUMETHBDX;
const int cNUMETHBDR = NUMETHBDR;
const int cNUMETHMCX = NUMETHMCX;
const int cNUMETHMCR = NUMETHMCR;
const int cNUMI2C = NUMI2C;
const int cINTxIRQ0 = INTxIRQ0;
const int cINTxIRQ1 = INTxIRQ1;
const int cINTxIRQ2 = INTxIRQ2;
const int cINTxIRQ3 = INTxIRQ3;
const int cINTUART0X = INTUART0X;
const int cINTUART0R = INTUART0R;
const int cINTUART1X = INTUART1X;
const int cINTUART1R = INTUART1R;
const int cINTGDMA0 = INTGDMA0;
const int cINTGDMA1 = INTGDMA1;
const int cINTTIMER0 = INTTIMER0;
const int cINTTIMER1 = INTTIMER1;
const int cINTHDLCAX = INTHDLCAX;
const int cINTHDLCAR = INTHDLCAR;
const int cINTHDLCBX = INTHDLCBX;
const int cINTHDLCBR = INTHDLCBR;
const int cINTETHBDX = INTETHBDX;
const int cINTETHBDR = INTETHBDR;
const int cINTETHMCX = INTETHMCX;
const int cINTETHMCR = INTETHMCR;
const int cINTI2C = INTI2C;

const unsigned int * cINTPND = INTPND;

const unsigned int * cINTMSK = INTMSK;
const int cGBLINTMSK = GBLINTMSK;

const unsigned int * cINTPRI0 = INTPRI0;
const int cNUMPRI0 = NUMPRI0;
const int cNUMPRI1 = NUMPRI1;
const int cNUMPRI2 = NUMPRI2;
const int cNUMPRI3 = NUMPRI3;

const unsigned int * cINTPRI1 = INTPRI1;
const int cNUMPRI4 = NUMPRI4;
const int cNUMPRI5 = NUMPRI5;
const int cNUMPRI6 = NUMPRI6;
const int cNUMPRI7 = NUMPRI7;

const unsigned int * cINTPRI2 = INTPRI2;
const int cNUMPRI8 = NUMPRI8;
const int cNUMPRI9 = NUMPRI9;
const int cNUMPRI10 = NUMPRI10;
const int cNUMPRI11 = NUMPRI11;

const unsigned int * cINTPRI3 = INTPRI3;
const int cNUMPRI12 = NUMPRI12;
const int cNUMPRI13 = NUMPRI13;
const int cNUMPRI14 = NUMPRI14;
const int cNUMPRI15 = NUMPRI15;

const unsigned int * cINTPRI4 = INTPRI4;
const int cNUMPRI16 = NUMPRI16;
const int cNUMPRI17 = NUMPRI17;
const int cNUMPRI18 = NUMPRI18;
const int cNUMPRI19 = NUMPRI19;

const unsigned int * cINTPRI5 = INTPRI5;
const int cNUMPRI20 = NUMPRI20;

const unsigned int * cINTOFFSET = INTOFFSET;
const unsigned int * cINTOSET_FIQ = INTOSET_FIQ;
const unsigned int * cINTOSET_IRQ = INTOSET_IRQ;

const unsigned int cNUMDIP4    = 3;
const unsigned int cNUMDIP3    = 2;
const unsigned int cNUMDIP2    = 1;
const unsigned int cNUMDIP1    = 0;
const unsigned int cDIP4       = (1 << NUMDIP4);
const unsigned int cDIP3       = (1 << NUMDIP3);
const unsigned int cDIP2       = (1 << NUMDIP2);
const unsigned int cDIP1       = (1 << NUMDIP1);

const unsigned int cNUMLED4    = 4;
const unsigned int cNUMLED3    = 5;
const unsigned int cNUMLED2    = 6;
const unsigned int cNUMLED1    = 7;
const unsigned int cLED4       = (1 << NUMLED4);
const unsigned int cLED3       = (1 << NUMLED3);
const unsigned int cLED2       = (1 << NUMLED2);
const unsigned int cLED1       = (1 << NUMLED1);

const unsigned int cNUMUSW3    = 8;
const unsigned int cUSRSW3     = (1 << NUMUSW3);

const unsigned int cNUMSEGA    = 10;
const unsigned int cNUMSEGB    = 11;
const unsigned int cNUMSEGC    = 12;
const unsigned int cNUMSEGD    = 13;
const unsigned int cNUMSEGE    = 14;
const unsigned int cNUMSEGG    = 15;
const unsigned int cNUMSEGF    = 16;
const unsigned int cSEGA       = (1 << NUMSEGA);
const unsigned int cSEGB       = (1 << NUMSEGB);
const unsigned int cSEGC       = (1 << NUMSEGC);
const unsigned int cSEGD       = (1 << NUMSEGD);
const unsigned int cSEGE       = (1 << NUMSEGE);
const unsigned int cSEGF       = (1 << NUMSEGF);
const unsigned int cSEGG       = (1 << NUMSEGG);

const unsigned int cINUSW3     = ~(0x1f << CONxIRQ0);
const unsigned int cIRQUSW3    = (0x19 << CONxIRQ0);

const unsigned int cOUTSEGA    = ~(0x1f << CONxIRQ2);
const unsigned int cOUTSEGB    = ~(0x1f << CONxIRQ3);
const unsigned int cOUTSEGC    = ~(0x3 << CONDRQ0);
const unsigned int cOUTSEGD    = ~(0x3 << CONDRQ1);
const unsigned int cOUTSEGE    = ~(0x2 << CONDAK0);
const unsigned int cOUTSEGF    = ~(0x1 << CONTOEN0);
const unsigned int cOUTSEGG    = ~(0x2 << CONDAK1);
