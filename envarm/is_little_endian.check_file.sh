#!/bin/bash


is_little=`${ARM_READELF} -h $1 | grep "endian" | grep "little"  | wc -l`
if [ $is_little == 0 ]
then
  echo
  echo  "WARNING ON" 
  echo               $1 " was compiled for BIG ENDIAN"
  echo  "WARNING OFF" 
  echo
  exit 1;
fi
exit 0;
