#!/bin/bash
EXOBJ=./exemplesObj
ENVARMPATH=./envarm
ENVGNUARM=/opt/gnu
if [ ! -d $ENVGNUARM ]; then
    echo "Tentative de copie de $ENVARMPATH/arm vers $ENVGNUARM"
    su -c "mkdir -p $ENVGNUARM && cp -r $ENVARMPATH/arm $ENVGNUARM"
fi
export PATH=$PATH:$ENVGNUARM/arm/bin
cd $ENVARMPATH
make
cd ..
source $ENVARMPATH/setenvarm.sh
cd $EXOBJ
./configure
make
cd ..
