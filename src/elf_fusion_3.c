#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "elf_header.h"
#include "elf_sectiontable.h"
#include "elf_fusionrenumsect.h"
#include "elf_relocationtable.h"
int main(int argc, char **argv)
{
    FILE *obj1;
    FILE *obj2;
    /*FILE* obj_out;*/

    Elf32_Ehdr *header1;
    Elf32_Ehdr *header2;

    Elf32_Shdr *sh_table1;
    Elf32_Shdr *sh_table2;
    int sh_table1_size, sh_table2_size;

    char **sh_table1_names;
    char **sh_table2_names;

    Elf32_Shdr *sh_table_res;
    int sh_table_res_size;
    char **assoc_name;

    char **table_section;

    int *section_offset;
    int *sh_table1_ndx;
    int *sh_table2_ndx;

    int i,j,ret;
    unsigned int k, nbEntry = 0;
    Elf32_Rel *relTable = NULL;
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    if ( argc != 4 ) {
        printf("Error: syntax is\n> %s obj_1 obj_2 obj_out\n", argv[0]);
        return -1;
    }

    if (!(obj1 = fopen(argv[1], "r"))) {
        printf("Error: cannot open %s with mode \"w\"\n", argv[1]);
        return -1;
    }

    if (!(obj2 = fopen(argv[2], "r"))) {
        printf("Error: cannot open %s with mode \"w\"\n", argv[2]);
        return -1;
    }

    /*
    if (!(obj_out = fopen(argv[3], "w"))) {
      printf("Error: cannot open %s with mode \"r\"\n", argv[3]);
      return ;
    }
     */


    if ( !Elf32_verify_magic(obj1) ) {
        printf("Error: magic number doesn't match ELF format for %s\n", argv[1]);
        fclose(obj1);
        return -1;
    }

    if ( !Elf32_verify_magic(obj2) ) {
        printf("Error: magic number doesn't match ELF format for %s\n", argv[2]);
        fclose(obj2);
        return -1;
    }


    ret = Elf32_read_header(obj1, &header1);

    if ( ret == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF header\n", argv[1]);
        fclose(obj1);
        free(header1);
        return -1;
    } else if ( ret == ERRCODE_READFILE ) {
        printf("Error: when reading %s's ELF header\n", argv[1]);
        fclose(obj1);
        free(header1);
        return -1;
    }

    ret = Elf32_read_header(obj2, &header2);

    if ( ret == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF header\n", argv[2]);
        fclose(obj2);
        free(header2);
        return -1;
    } else if ( ret == ERRCODE_READFILE ) {
        printf("Error: when reading %s's ELF header\n", argv[2]);
        fclose(obj2);
        free(header2);
        return -1;
    }

    sh_table1_size = Elf32_read_sectiontable(obj1, header1, &sh_table1);
    sh_table2_size = Elf32_read_sectiontable(obj2, header2, &sh_table2);

    sh_table1_names = malloc(sizeof(char *) * sh_table1_size);

    for ( i = 0 ; i < sh_table1_size ; i++ ) {
        sh_table1_names[i] = Elf32_seek_nameshstr(obj1, Elf32_seek_shstr(header1, sh_table1), &sh_table1[i]);
    }

    sh_table2_names = malloc(sizeof(char *) * sh_table2_size);

    for ( i = 0 ; i < sh_table2_size ; i++ ) {
        sh_table2_names[i] = Elf32_seek_nameshstr(obj2, Elf32_seek_shstr(header2, sh_table2), &sh_table2[i]);
    }




    sh_table_res_size = Elf32_fusion_progbits(obj1, header1, sh_table1, sh_table1_names
                                              , obj2, header2, sh_table2, sh_table2_names
                                              , &sh_table_res, &assoc_name, &table_section, &section_offset
                                              , &sh_table1_ndx, &sh_table2_ndx
                                             );

    printf("FusOff\t| N\tName\t\tNamenxd\tType\t\tAddr.\tOffset\tSize\tEntSize\tFlags\tLink\tInfo\tAlign.\n");
    printf("\t| [0]\tNULL\n");

    for ( i = 1 ; i < sh_table_res_size ; i++ ) {
        printf("%d\t| [%d]\t", section_offset[i], i);

        printf("%s", assoc_name[i]);

        if ( strlen(assoc_name[i]) >= 8 ) {
            printf("\t");
        } else {
            printf("\t\t");
        }

        Elf32_print_sheader(&sh_table_res[i]);
    }



    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /*  Récupération des tables de relocation */
    for( i = 1; i < sh_table1_size; i++) {
        for(j = 1; j < sh_table2_size; j++) {
            if (sh_table1[i].sh_type == SHT_REL
                    && sh_table2[i].sh_type == SHT_REL
                    && strcmp(sh_table2_names[i], sh_table1_names[j]) == 0) {
                printf("Correspondance trouvé pour la table %s\n",sh_table1_names[j]);
                sh_table_res[sh_table2_ndx[j]].sh_flags |= sh_table1[i].sh_flags;
                sh_table_res[sh_table2_ndx[j]].sh_size += sh_table1[i].sh_size;
                section_offset[sh_table1_ndx[j]] = sh_table2[i].sh_size;
                table_section[sh_table2_ndx[j]] = realloc(table_section[sh_table2_ndx[j]], sh_table_res[sh_table2_ndx[j]].sh_size);
                memcpy(table_section[sh_table2_ndx[j]] + section_offset[sh_table2_ndx[j]] , table_section[sh_table1_ndx[i]],sh_table1[i].sh_size);
                sh_table1_ndx[i] = -sh_table1_ndx[i];
                printf("Entrées de la section %u\n", sh_table_res[sh_table2_ndx[j]].sh_name);
                nbEntry = sh_table_res[sh_table2_ndx[j]].sh_size / sh_table_res[sh_table2_ndx[j]].sh_entsize;
                printf("Nombre d'entrée : %u\n", nbEntry);

                for ( k = 0 ; k < nbEntry; k++) {
                    relTable = malloc(sizeof(Elf32_Rel));
                    printf("memcpy...");
                    fflush(stdout);
                    memcpy(relTable, table_section[sh_table2_ndx[j]] + k,sizeof(Elf32_Rel));
                    printf("OK\n");
                    fflush(stdout);
                    printf("Relocation table entry %d\n", k);
                    printf("> offset : %x\n", relTable->r_offset);
                    printf("> info : %x\n", relTable->r_info);
                    printf("> type : %s\n", rtype_str(ELF32_R_TYPE(relTable->r_info)));
                    printf("> val sym : %d\n\n", ELF32_R_SYM(relTable->r_info));
                    free(relTable);
                }

                printf("******\n");
                fflush(stdout);
                break;
            }
        }
    }

    fclose(obj1);
    fclose(obj2);

    free(header1);
    free(header2);

    free(sh_table1);
    free(sh_table2);
    free(sh_table_res);
    free(assoc_name);

    for ( i = 0 ; i < sh_table1_size ; i++ ) {
        free(sh_table1_names[i]);
    }

    free(sh_table1_names);

    for ( i = 0 ; i < sh_table2_size ; i++ ) {
        free(sh_table2_names[i]);
    }

    free(sh_table2_names);

    for ( i = 0 ; i < sh_table_res_size ; i++ ) {
        free(table_section[i]);
    }

    free(table_section);

    free(section_offset);
    free(sh_table1_ndx);
    free(sh_table2_ndx);

    return 0;
}

