#include <stdio.h>
#include <string.h>
#include "types.h"
#include "elf_header.h"
#include "elf_sectiontable.h"
#include "elf_symboltable.h"
#include "elf_readsect.h"

int main(int argc, char **argv)
{
    int i, j, ret, sh_table_size;
    FILE *objfile;
    Elf32_Ehdr *header;
    Elf32_Shdr *sh_table = NULL;
    Elf32_Sym *sym_table = NULL;

    for (i = 1; i < argc; i++) {
        if (!(objfile = fopen(argv[i], "r"))) {
            printf("Error: %s is not a file\n", argv[i]);
            continue;
        }

        if (!Elf32_verify_magic(objfile)) {
            printf("Error: magic number doesn't match ELF format for %s\n", argv[i]);
            continue;
        }

        ret = Elf32_read_header(objfile, &header);

        if (ret == ERRCODE_BADALLOC) {
            printf("Error: allocation failure for %s's ELF header\n", argv[i]);
            continue;
        } else if (ret == ERRCODE_READFILE) {
            printf("Error: when reading %s's ELF header\n", argv[i]);
            continue;
        }

        printf("--[ELF section table for %s]\n", argv[i]);
        sh_table_size = Elf32_read_sectiontable(objfile, header, &sh_table);

        if (sh_table_size != header->e_shnum) {
            printf
            ("Error: when trying to parse section header table (errcode %d)\n", sh_table_size);
            break;
        }

        printf("N\tNameId\tType\t\tAddr.\tOffset\tSize\tEntSize\tFlags\tLink\tInfo\tAlign.\n");

        for (j = 0; j < sh_table_size; j++) {
            printf("[%d]\t", j);
            Elf32_print_sheader(&(sh_table[j]));
        }

        printf("\n");
        printf("\n");
        char *str_tab;

        int index = Elf32_seek_indexsymtab(header, sh_table);

        int test=Elf32_read_symtb_strtb(objfile, sh_table+index, &sh_table, &str_tab);

        if( test == ERRCODE_BADALLOC ) {
            printf("Error: allocation failure for %s's ELF string table\n", argv[i]);
            continue;
        }

        if( test == ERRCODE_READFILE ) {
            printf("Error: reading failure for %s's ELF string table\n", argv[i]);
            continue;
        }

        int nb = Elf32_read_symtb(objfile, sh_table + index, &sym_table);

        if( nb == ERRCODE_READFILE ) {
            printf("Error: reading failure for %s's ELF sym table\n", argv[i]);
            continue;
        }

        if( nb == ERRCODE_BADALLOC ) {
            printf("Error: allocation failure for %s's ELF sym table\n", argv[i]);
            continue;
        }

        Elf32_print_symtb(sym_table, sh_table+index, nb, &str_tab);


        fclose(objfile);
        free(header);
        free(sh_table);
        //free(str_tab);
    }

    return 0;
}
