#include "elf_progbits.h"
#include "elf_sectiontable.h"

/*fonction de concatenation*/
int concatsection(Elf32_Shdr *section1, char **sect1, Elf32_Shdr *section2, char **sect2)
{
    int concat;
    /*on realloue de la mémoire pour concatener les contenus des sections 1 et 2*/
    sect1=realloc(sect1, sizeof(char)*section2->sh_size);
    /*on concatene le contenu des headers des sections 1 et 2.*/
    concat=concatheader(section1, section2);

    /*concat doit renvoyer un code d'erreur si la concatenation des headers ne s'est pas faite.*/
    /*code d'erreur temporaire*/
    if (concat!=0) {
        return -1;
    }

    return 0;
}

/*fonction d'identification du nom de la section (?)*/
int stringtable_section_name(FILE *file, Elf32_Shdr **sect_table, Elf32_Ehdr *header, char **stringtable)
{
    Elf32_Shdr *str_tab_header = Elf32_seek_shstr(header, *sect_table);
    /*prototype de la fonction : Elf32_Shdr *Elf32_seek_shstr(Elf32_Ehdr *header, Elf32_Shdr *sh_table);*/

    malloc(sizeof(char) *str_tab_header->sh_size );

    Elf32_read_sect(file, *sect_table, stringtable);
    /*prototype de la fonction : int Elf32_read_sect(FILE *file, Elf32_Shdr *header, char **sect);*/

    return 0;
}

/*recherche de la section*/
int findsection(Elf32_Shdr **sect_table, char **string_table, char *name, int nb_sh)
{
    char *getname(Elf32_Shdr *sect, char **string_table) {
        return string_table[sect->sh_name] ;
    }

    int i;

    for(i=0; i<nb_sh; i++) {
        if (name==getname(sect_table[i], string_table)) {
            return i;
        }
    }

    return -1;

    return 0;
}
/*recherche de la section essai 2*/
int find(Elf32_Shdr *section2, Elf32_Shdr **shtable1, FILE *file1, Elf32_Ehdr *header1)
{
    /*sname = Elf32_seek_nameshstr(file1, Elf32_seek_shstr(header, sh_table), &sh_table[j]);

        char* Elf32_seek_nameshstr(FILE* file, Elf32_Shdr* shstr, Elf32_Shdr* sheader);
    Elf32_Shdr* Elf32_seek_shstr(Elf32_Ehdr* header, Elf32_Shdr* sh_table);
        */
    return 0;
}


/*ajoute une section dans un fichier*/
/* non prioritaire */
int writesection(Elf32_Shdr *section1, char **sect1, FILE *result)
{
    /*fprintf(file, *sect1);*/
    return 0;
}

/*ajoute un header dans la table des sections*/
int addheader(Elf32_Shdr *section1, Elf32_Shdr **shtable, int nb)
{
    /*shtable=realloc(shtable, sizeof(Elf32_Shdr));
    (*shtable)[nb]= *Elf32_Shdr;*/
    return 0;
}

/*concataine deux headers avec PROGBITS en un seul header dans une table des symboles.*/
/*section1 est le paramètre-résultat.*/
int concatheader(Elf32_Shdr *section1, Elf32_Shdr *section2)
{
    section1->sh_size = section1->sh_size + section2->sh_size;
    return 0;
}
