#include <stdio.h>
#include "types.h"
#include "elf_header.h"
#include "elf_sectiontable.h"
#include "elf_relocationtable.h"

int main(int argc, char **argv)
{
    int i, j, k, ret, sh_table_size;
    FILE *objfile;
    Elf32_Ehdr *header;
    Elf32_Shdr *sh_table = NULL;
    Elf32_Shdr *sh_reltable = NULL;
    Elf32_Rel *rel;

    for (i = 1; i < argc; i++) {
        if (!(objfile = fopen(argv[i], "r"))) {
            printf("Error: %s is not a file\n", argv[i]);
            continue;
        }

        if (!Elf32_verify_magic(objfile)) {
            printf("Error: magic number doesn't match ELF format for %s\n", argv[i]);
            continue;
        }

        ret = Elf32_read_header(objfile, &header);

        if (ret == ERRCODE_BADALLOC) {
            printf("Error: allocation failure for %s's ELF header\n", argv[i]);
            fclose(objfile);
            continue;
        } else if (ret == ERRCODE_READFILE) {
            printf("Error: when reading %s's ELF header\n", argv[i]);
            fclose(objfile);
            continue;
        }

        printf("--[ELF section table for %s]\n", argv[i]);
        sh_table_size = Elf32_read_sectiontable(objfile, header, &sh_table);

        if (sh_table_size != header->e_shnum) {
            printf("Error: when trying to parse section header table (errcode %d)\n", sh_table_size);
            fclose(objfile);
            free(header);
            continue;
        }

        printf("N\tNameId\tType\t\tAddr.\tOffset\tSize\tEntSize\tFlags\tLink\tInfo\tAlign.\n");

        for (j = 0; j < sh_table_size; j++) {
            printf("[%d]\t", j);
            Elf32_print_sheader(&(sh_table[j]));
        }

        printf("\n--[Reading Relocation tables]--\n");
        sh_table_size = Elf32_read_relocationtable(header, sh_table, &sh_reltable);

        if( sh_table_size == ERRCODE_BADALLOC ) {
            printf("Error: allocation failure for %s's ELF string table\n", argv[i]);
            fclose(objfile);
            free(header);
            free(sh_table);
            continue;
        }

        for ( j = 0 ; j < sh_table_size ; j++ ) {
            ret = Elf32_seek_rel(objfile, sh_reltable + j, &rel);

            if ( ret < 0 ) {
                printf("Error: when trying to parse relocation table (errcode %d)\n", ret);
                continue;
            }

            for ( k = 0 ; k < ret ; k++ ) {
                printf("Relocation table %d, entry %d\n", j, k);
                printf("> offset : %x\n", rel[k].r_offset);
                printf("> info : %x\n", rel[k].r_info);
                printf("> type : %s\n", rtype_str(ELF32_R_TYPE(rel[k].r_info)));
                printf("> val sym : %d\n\n", ELF32_R_SYM(rel[k].r_info));
            }

            free(rel);
        }

        fclose(objfile);
        free(sh_reltable);
        free(header);
        free(sh_table);
    }

    return 0;
}
