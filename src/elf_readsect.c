#include "elf_readsect.h"



/*lis une section correspondant à son header*/
int Elf32_read_sect(FILE *file, Elf32_Shdr *header, char **sect)
{
    fseek(file, header->sh_offset, SEEK_SET);
    *sect = malloc(header->sh_size);

    if (sect == NULL) {
        return ERRCODE_BADALLOC ;
    }

    fread(*sect, 1, header->sh_size, file);

    return 0;
}
/*
 * lis une section correspondant à sont header int Elf32_read_sect(FILE*
 * file, Elf32_Shdr * tab, char * sect, char * name) { return 0 ; } lis une
 * section correspondant à sont header int Elf32_read_sect(FILE* file,
 * Elf32_Shdr * tab, char * sect, int number) { return 0 ; } */
/* affiche une section en Héxa */
void Elf32_printf_sect(FILE *file, Elf32_Shdr *header)
{
    unsigned int a=0;
    int i,j=0, k=0, test;
    unsigned int adr;
    adr = header->sh_offset ;
    printf("affichage de la section : \n");
    fseek(file, header->sh_offset, SEEK_SET);
    int size = header->sh_size;

    printf("taille de la section : %d o \n", size);
    printf("%x \t", adr);

    for(i=0; i< size ; i++) //boucle d'affichage pas super propre pour faire un truc qui correspond a Hexdump
        // correspond a Hexdump
    {
        test=fread(&a, sizeof(char), 1, file);

        if (test==0) {
            printf("ERRCODE_READFAIL\n");
        }

        if (a < 15) {
            printf("0");
        }

        printf("%x", a);

        j++;

        if (j == 2) {
            printf(" ");
            k++;

            if (k == 8) {
                adr=adr+16;
                printf(" \n");
                printf("%x \t", adr);
                k = 0;
            }

            j = 0;
        }
    }

    printf(" \n");
}
