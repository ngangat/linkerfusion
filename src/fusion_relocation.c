#include <stdio.h>
#include "types.h"
#include "elf_header.h"
#include "elf_sectiontable.h"
#include "elf_relocationtable.h"
int main(int argc, char **argv)
{
    int j, k, ret, sh_table_size1,sh_table_size2;
    FILE *objfile1,*objfile2;
    Elf32_Ehdr *header1, *header2;
    Elf32_Shdr *sh_table1 = NULL, *sh_table2 = NULL;
    Elf32_Shdr *sh_reltable1 = NULL, *sh_reltable2 = NULL;
    Elf32_Rel *rel1 = NULL, *rel2 = NULL ;

    int sh_table_size3 = 0;
    FILE *objfile3 = NULL;
    Elf32_Ehdr *header3 = NULL;
    Elf32_Shdr *sh_table3 = NULL;
    Elf32_Shdr *sh_reltable3 = NULL;
    Elf32_Rel *rel3 = NULL;


    if(argc != 4) {
        printf("Error :\nUsage : %s fichier1.o fichier2.o result.o\n",argv[0]);
    }

    /* Ouverture fichier objet 1 et tests */
    if (!(objfile1 = fopen(argv[1], "r"))) {
        printf("Error: %s is not a file\n", argv[1]);
        return EXIT_FAILURE;
    }

    if (!Elf32_verify_magic(objfile1)) {
        printf("Error: magic number doesn't match ELF format for %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    /* Ouverture fichier objet 2 et tests */
    if (!(objfile2 = fopen(argv[2], "r"))) {
        printf("Error: %s is not a file\n", argv[2]);
        return EXIT_FAILURE;
    }

    if (!Elf32_verify_magic(objfile2)) {
        printf("Error: magic number doesn't match ELF format for %s\n", argv[2]);
        return EXIT_FAILURE;
    }

    /* Lecture header fichier objet 1 et tests */
    ret = Elf32_read_header(objfile1, &header1);

    if (ret == ERRCODE_BADALLOC) {
        printf("Error: allocation failure for %s's ELF header\n", argv[1]);
        fclose(objfile1);
        return EXIT_FAILURE;
    } else if (ret == ERRCODE_READFILE) {
        printf("Error: when reading %s's ELF header\n", argv[1]);
        fclose(objfile1);
        return EXIT_FAILURE;
    }

    /* Lecture header fichier objet 2 et tests */
    ret = Elf32_read_header(objfile2, &header2);

    if (ret == ERRCODE_BADALLOC) {
        printf("Error: allocation failure for %s's ELF header\n", argv[2]);
        fclose(objfile2);
        return EXIT_FAILURE;
    } else if (ret == ERRCODE_READFILE) {
        printf("Error: when reading %s's ELF header\n", argv[2]);
        fclose(objfile2);
        return EXIT_FAILURE;
    }

    /* Lecture table section fichier objet 1 et tests */
    printf("--[ELF section table for %s]\n", argv[1]);
    sh_table_size1 = Elf32_read_sectiontable(objfile1, header1, &sh_table1);

    if (sh_table_size1 != header1->e_shnum) {
        printf("Error: when trying to parse section header table (errcode %d)\n", sh_table_size1);
        fclose(objfile1);
        free(header1);
        return EXIT_FAILURE;
    }

    /* Lecture table section fichier objet 2 et tests */
    printf("--[ELF section table for %s]\n", argv[2]);
    sh_table_size2 = Elf32_read_sectiontable(objfile2, header2, &sh_table2);

    if (sh_table_size2 != header2->e_shnum) {
        printf("Error: when trying to parse section header table (errcode %d)\n", sh_table_size2);
        fclose(objfile2);
        free(header2);
        return EXIT_FAILURE;
    }


    /* Lecture table relocation fichier objet 1 et tests */
    sh_table_size1 = Elf32_read_relocationtable(header1, sh_table1, &sh_reltable1);

    if( sh_table_size1 == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF string table\n", argv[1]);
        fclose(objfile1);
        free(header1);
        free(sh_table1);
        return EXIT_FAILURE;
    }

    sh_table_size3 = sh_table_size1;

    for ( j = 0 ; j < sh_table_size1 ; j++ ) {
        ret = Elf32_seek_rel(objfile1, sh_reltable1 + j, &rel1);

        if ( ret < 0 ) {
            printf("Error: when trying to parse relocation table (errcode %d)\n", ret);
            return EXIT_FAILURE;
        }

        for ( k = 0 ; k < ret ; k++ ) {
            printf("Relocation table %d, entry %d\n", j, k);
            printf("> offset : %x\n", rel1[k].r_offset);
            printf("> info : %x\n", rel1[k].r_info);
            printf("> type : %s\n", rtype_str(ELF32_R_TYPE(rel1[k].r_info)));
            printf("> val sym : %d\n\n", ELF32_R_SYM(rel1[k].r_info));
        }

        free(rel1);
    }

    /* Lecture table relocation fichier objet 1 et tests */
    sh_table_size2 = Elf32_read_relocationtable(header2, sh_table2, &sh_reltable2);

    if( sh_table_size2 == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF string table\n", argv[2]);
        fclose(objfile2);
        free(header2);
        free(sh_table2);
        return EXIT_FAILURE;
    }

    sh_table_size3 += sh_table_size2;
    sh_reltable3 = malloc(sizeof(Elf32_Shdr) * sh_table_size3);
    memcpy(sh_reltable3,sh_reltable1,sh_table_size1);
    memcpy(sh_reltable3+sh_table_size1,sh_reltable2,sh_table_size2);

    for ( j = 0 ; j < sh_table_size2 ; j++ ) {
        ret = Elf32_seek_rel(objfile2, sh_reltable2 + j, &rel2);

        if ( ret < 0 ) {
            printf("Error: when trying to parse relocation table (errcode %d)\n", ret);
            return EXIT_FAILURE;
        }

        for ( k = 0 ; k < ret ; k++ ) {
            printf("Relocation table %d, entry %d\n", j, k);
            printf("> offset : %x\n", rel2[k].r_offset);
            printf("> info : %x\n", rel2[k].r_info);
            printf("> type : %s\n", rtype_str(ELF32_R_TYPE(rel2[k].r_info)));
            printf("> val sym : %d\n\n", ELF32_R_SYM(rel2[k].r_info));
        }

        free(rel2);
    }

    fclose(objfile1);
    free(sh_reltable1);
    free(header1);
    free(sh_table1);

    fclose(objfile2);
    free(sh_reltable2);
    free(header2);
    free(sh_table2);


    return 0;
}
