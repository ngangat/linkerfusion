#include "elf_sectiontable.h"
#include "elf_readsect.h"

#define EN_SHT_NULL "NULL\t\t"
#define EN_SHT_PROGBITS "PROGBITS\t"
#define EN_SHT_SYMTAB "SYMTAB\t\t"
#define EN_SHT_STRTAB "STRTAB\t\t"
#define EN_SHT_RELA "RELA\t\t"
#define EN_SHT_HASH "HASH\t\t"
#define EN_SHT_DYNAMIC "DYNAMIC\t\t"
#define EN_SHT_NOTE "NOTE\t\t"
#define EN_SHT_NOBITS "NOBITS\t\t"
#define EN_SHT_REL "REL\t\t"
#define EN_SHT_SHLIB "SHLIB\t\t"
#define EN_SHT_DYNSYM "DYNSYM\t\t"
#define EN_SHT_LOPROC "LOPROC\t\t"
#define EN_SHT_HIPROC "HIPROC\t\t"
#define EN_SHT_LOUSER "LOUSER\t\t"
#define EN_SHT_HIUSER "HIUSER\t\t"

int Elf32_read_sectiontable(FILE *file, Elf32_Ehdr *header,
                            Elf32_Shdr **sh_table)
{
    Elf32_Off sh_table_off = header->e_shoff;
    Elf32_Half sh_table_num = header->e_shnum;
    int i;
    Elf32_Shdr *ptr_shtable;

    if (sh_table_off <= 0) {
        return ERRCODE_NOENTRY;
    }

    *sh_table = malloc(sizeof(Elf32_Shdr) * sh_table_num);

    if (*sh_table == NULL) {
        return ERRCODE_BADALLOC;
    }

    ptr_shtable = *sh_table;

    if (fseek(file, sh_table_off, SEEK_SET) != 0) {
        return ERRCODE_BADMOVE;
    }

    for (i = 0; i < sh_table_num; i++) {
        fread(&(ptr_shtable[i].sh_name), sizeof(Elf32_Word), 1, file);
        fread(&(ptr_shtable[i].sh_type), sizeof(Elf32_Word), 1, file);
        fread(&(ptr_shtable[i].sh_flags), sizeof(Elf32_Word), 1, file);
        fread(&(ptr_shtable[i].sh_addr), sizeof(Elf32_Addr), 1, file);
        fread(&(ptr_shtable[i].sh_offset), sizeof(Elf32_Off), 1, file);
        fread(&(ptr_shtable[i].sh_size), sizeof(Elf32_Word), 1, file);
        fread(&(ptr_shtable[i].sh_link), sizeof(Elf32_Word), 1, file);
        fread(&(ptr_shtable[i].sh_info), sizeof(Elf32_Word), 1, file);
        fread(&(ptr_shtable[i].sh_addralign), sizeof(Elf32_Word), 1, file);

        if (!fread(&(ptr_shtable[i].sh_entsize), sizeof(Elf32_Word), 1, file)) {
            free(ptr_shtable);
            *sh_table = NULL;
            return ERRCODE_READFILE;
        }
    }

    return sh_table_num;
}

int Elf32_print_sheader(Elf32_Shdr *sheader)
{
    char *type;

    if (sheader == NULL) {
        return ERRCODE_SHNULL;
    }

    if (sheader->sh_type == SHT_PROGBITS) {
        type = EN_SHT_PROGBITS;
    } else if (sheader->sh_type == SHT_SYMTAB) {
        type = EN_SHT_SYMTAB;
    } else if (sheader->sh_type == SHT_STRTAB) {
        type = EN_SHT_STRTAB;
    } else if (sheader->sh_type == SHT_RELA) {
        type = EN_SHT_RELA;
    } else if (sheader->sh_type == SHT_HASH) {
        type = EN_SHT_HASH;
    } else if (sheader->sh_type == SHT_DYNAMIC) {
        type = EN_SHT_DYNAMIC;
    } else if (sheader->sh_type == SHT_NOTE) {
        type = EN_SHT_NOTE;
    } else if (sheader->sh_type == SHT_NOBITS) {
        type = EN_SHT_NOBITS;
    } else if (sheader->sh_type == SHT_REL) {
        type = EN_SHT_REL;
    } else if (sheader->sh_type == SHT_SHLIB) {
        type = EN_SHT_SHLIB;
    } else if (sheader->sh_type == SHT_DYNSYM) {
        type = EN_SHT_DYNSYM;
    } else if (sheader->sh_type == SHT_LOPROC) {
        type = EN_SHT_LOPROC;
    } else if (sheader->sh_type == SHT_HIPROC) {
        type = EN_SHT_HIPROC;
    } else if (sheader->sh_type == SHT_LOUSER) {
        type = EN_SHT_LOUSER;
    } else if (sheader->sh_type == SHT_HIUSER) {
        type = EN_SHT_HIUSER;
    } else {
        type = EN_SHT_NULL;
    }

    printf("%d\t%s%x\t%x\t%x\t%d\t%x\t%d\t%d\t%d\n"
           , sheader->sh_name
           , type
           , sheader->sh_addr
           , sheader->sh_offset
           , sheader->sh_size
           , sheader->sh_entsize
           , sheader->sh_flags
           , sheader->sh_link
           , sheader->sh_info
           , sheader->sh_addralign
          );

    return 0;
}

int Elf32_seek_indexsymtab(Elf32_Ehdr *header, Elf32_Shdr *sheader)
{
    int i;

    for (i = 0; i < header->e_shnum; i++)
        if (sheader[i].sh_type == SHT_SYMTAB) {
            return i;
        }

    return -1;
}

Elf32_Shdr *Elf32_seek_shstr(Elf32_Ehdr *header, Elf32_Shdr *sh_table)
{
    if ( header->e_shstrndx == SHN_UNDEF ) {
        return NULL;
    } else {
        return sh_table + header->e_shstrndx;
    }
}

char *Elf32_seek_nameshstr(FILE *file, Elf32_Shdr *shstr, Elf32_Shdr *sheader)
{
    char *sstr;
    char *namestr;

    Elf32_read_sect(file, shstr, &sstr);
    namestr = malloc(sizeof(char) * strlen(&(sstr[sheader->sh_name])));

    if ( namestr == NULL ) {
        return "\0";
    }

    strcpy(namestr, &(sstr[sheader->sh_name]));

    free(sstr);

    return namestr;
}
