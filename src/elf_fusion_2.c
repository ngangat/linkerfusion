#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "elf_header.h"
#include "elf_sectiontable.h"
#include "elf_fusionrenumsect.h"
#include "elf_symboltable.h"
#include "elf_fusion_sym.h"
#include "elf_fusion_sym.h"

int main(int argc, char **argv)
{
    FILE *obj1;
    FILE *obj2;
    /*FILE* obj_out;*/

    Elf32_Ehdr *header1;
    Elf32_Ehdr *header2;

    Elf32_Shdr *sh_table1;
    Elf32_Shdr *sh_table2;
    int sh_table1_size, sh_table2_size;

    char **sh_table1_names;
    char **sh_table2_names;

    Elf32_Shdr *sh_table_res;
    int sh_table_res_size;
    char **assoc_name;

    char **table_section;

    int *section_offset;
    int *sh_table1_ndx;
    int *sh_table2_ndx;

    int i, ret;

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    if ( argc != 4 ) {
        printf("Error: syntax is\n> %s obj_1 obj_2 obj_out\n", argv[0]);
        return -1;
    }

    if (!(obj1 = fopen(argv[1], "r"))) {
        printf("Error: cannot open %s with mode \"w\"\n", argv[1]);
        return -1;
    }

    if (!(obj2 = fopen(argv[2], "r"))) {
        printf("Error: cannot open %s with mode \"w\"\n", argv[2]);
        return -1;
    }

    /*
    if (!(obj_out = fopen(argv[3], "w"))) {
      printf("Error: cannot open %s with mode \"r\"\n", argv[3]);
      return ;
    }
     */


    if ( !Elf32_verify_magic(obj1) ) {
        printf("Error: magic number doesn't match ELF format for %s\n", argv[1]);
        fclose(obj1);
        return -1;
    }

    if ( !Elf32_verify_magic(obj2) ) {
        printf("Error: magic number doesn't match ELF format for %s\n", argv[2]);
        fclose(obj2);
        return -1;
    }


    ret = Elf32_read_header(obj1, &header1);

    if ( ret == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF header\n", argv[1]);
        fclose(obj1);
        free(header1);
        return -1;
    } else if ( ret == ERRCODE_READFILE ) {
        printf("Error: when reading %s's ELF header\n", argv[1]);
        fclose(obj1);
        free(header1);
        return -1;
    }

    ret = Elf32_read_header(obj2, &header2);

    if ( ret == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF header\n", argv[2]);
        fclose(obj2);
        free(header2);
        return -1;
    } else if ( ret == ERRCODE_READFILE ) {
        printf("Error: when reading %s's ELF header\n", argv[2]);
        fclose(obj2);
        free(header2);
        return -1;
    }

    sh_table1_size = Elf32_read_sectiontable(obj1, header1, &sh_table1);
    sh_table2_size = Elf32_read_sectiontable(obj2, header2, &sh_table2);

    sh_table1_names = malloc(sizeof(char *) * sh_table1_size);

    for ( i = 0 ; i < sh_table1_size ; i++ ) {
        sh_table1_names[i] = Elf32_seek_nameshstr(obj1, Elf32_seek_shstr(header1, sh_table1), &sh_table1[i]);
    }

    sh_table2_names = malloc(sizeof(char *) * sh_table2_size);

    for ( i = 0 ; i < sh_table2_size ; i++ ) {
        sh_table2_names[i] = Elf32_seek_nameshstr(obj2, Elf32_seek_shstr(header2, sh_table2), &sh_table2[i]);
    }




    sh_table_res_size = Elf32_fusion_progbits(obj1, header1, sh_table1, sh_table1_names
                                              , obj2, header2, sh_table2, sh_table2_names
                                              , &sh_table_res, &assoc_name, &table_section, &section_offset
                                              , &sh_table1_ndx, &sh_table2_ndx
                                             );

    printf("\n");
    printf("************************************************************\n");
    printf("******************** TABLE SDES SECTION ********************\n");
    printf("************************************************************\n");
    printf("\n");

    printf("FusOff\t| N\tName\t\tNamenxd\tType\t\tAddr.\tOffset\tSize\tEntSize\tFlags\tLink\tInfo\tAlign.\n");
    printf("\t| [0]\tNULL\n");

    for ( i = 1 ; i < sh_table_res_size ; i++ ) {
        printf("%d\t| [%d]\t", section_offset[i], i);

        printf("%s", assoc_name[i]);

        if ( strlen(assoc_name[i]) >= 8 ) {
            printf("\t");
        } else {
            printf("\t\t");
        }

        Elf32_print_sheader(&sh_table_res[i]);
    }

    /* ************************************************************** */
    /* ************************************************************** */
    /* ************************************************************** */

    char *str_tab1;
    char *str_tab2;
    Elf32_Sym *sym_table1;
    Elf32_Sym *sym_table2;
    Elf32_Shdr *sh_symtb1;
    Elf32_Shdr *sh_symtb2;
    /* LECTURE ET ECRITURE DE LA TABLE DES SYMBOLES POUR LE FICHIER 1 */
    int index1 = Elf32_seek_indexsymtab(header1, sh_table1);
    sh_symtb1=sh_table1+index1 ;
    /* LECTURE de la table des string du fichier 1 */
    int test1 = Elf32_read_symtb_strtb(obj1, sh_table1 + index1, &sh_table1, &str_tab1);

    if( test1 == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF string table\n", argv[1]);
        return 0;
    }

    if( test1 == ERRCODE_READFILE ) {
        printf("Error: reading failure for %s's ELF string table\n", argv[1]);
        return 0;
    }

    /* LECTURE de la table des symboles du fichier 1 */
    int nb1 = Elf32_read_symtb(obj1, sh_table1 + index1, &sym_table1);

    if( nb1 == ERRCODE_READFILE ) {
        printf("Error: reading failure for %s's ELF sym table\n", argv[1]);
        return 0;
    }

    if( nb1 == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF sym table\n", argv[1]);
        return 0;
    }


    /* LECTURE ET ECRITURE DE LA TABLE DES SYMBOLES POUR LE FICHIER 2 */
    int index2 = Elf32_seek_indexsymtab(header2, sh_table2);
    sh_symtb2=sh_table2+index2 ;
    /* LECTURE de la table des string du fichier 2 */
    int test2 = Elf32_read_symtb_strtb(obj2, sh_table2 + index2, &sh_table2, &str_tab2);

    if( test2 == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF string table\n", argv[2]);
        return 0;
    }

    if( test1 == ERRCODE_READFILE ) {
        printf("Error: reading failure for %s's ELF string table\n", argv[2]);
        return 0;
    }

    /* LECTURE de la table des symboles du fichier 2 */
    int nb2 = Elf32_read_symtb(obj2, sh_table2 + index2, &sym_table2);

    if( nb2 == ERRCODE_READFILE ) {
        printf("Error: reading failure for %s's ELF sym table\n", argv[2]);
        return 0;
    }

    if( nb2 == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF sym table\n", argv[2]);
        return 0;
    }

    /* fusionne deux tables des strings */
    /* paramètre resultat: table1, header1 : paramètre table2 header2 */
    /* code d'erreur éventuel / offset des string */
    /* alloue de la mémoire */
    int chgmnt ;
    chgmnt =fusion_stringtable((sh_table1)+(sh_symtb1->sh_link), &str_tab1, (sh_table2)+(sh_symtb2->sh_link), &str_tab2);
    //printf("fusion strtab effectuer : %d \n", chgmnt);

    /* met a jour les adresse des nom */
    /* paramètre resultat: table1: paramètre nb (offset a appliquer)*/
    /* code d'erreur éventuel  */
    /* */
    int test;
    test= fusion_modifname(chgmnt, &sym_table2, nb2);
    //printf("application de la modification de l'offset aux noms : %d \n", test);

    /* met a jour les adresse des symboles dans les fonctions */
    /* paramètre resultat: table1 : paramètre offset*/
    /* code d'erreur éventuel  */
    /* */
    test = fusion_modif_offset(&sym_table2, nb2, section_offset);
    //printf("modification des adresses des symboles : %d \n", test);



    /* met a jour les adresse des symboles dans les fonctions */
    /* paramètre resultat: table1 : paramètre offset*/
    /* code d'erreur éventuel  */
    /* */
    test = fusion_maj_shndx(&sym_table1, nb1, sh_table1_ndx);
    test = fusion_maj_shndx(&sym_table2, nb2, sh_table2_ndx);
    /* fusionne les tables des symboles */
    /* paramètre resultat: table1 : paramètre offset*/
    /* code d'erreur éventuel  */
    /* */

    printf("\n");
    printf("*********************************************************\n");
    printf("******************** TABLE SYMBOLE 1 ********************\n");
    printf("*********************************************************\n");
    printf("\n");

    Elf32_print_symtb(sym_table1, sh_table1 + index1, nb1, &str_tab1);

    printf("\n");
    printf("*********************************************************\n");
    printf("******************** TABLE SYMBOLE 2 ********************\n");
    printf("*********************************************************\n");
    printf("\n");

    Elf32_print_symtb(sym_table2, sh_table2 + index2, nb2, &str_tab1);

    test = fusionner_table( &sym_table1, &sym_table2, sh_symtb1, sh_symtb2, &str_tab1, nb1, nb2);

    printf("\n");
    printf("*****************************************************************\n");
    printf("******************** TABLE SYMBOLE FUSIONNEE ********************\n");
    printf("*****************************************************************\n");
    printf("\n");

    Elf32_print_symtb(sym_table1, sh_table1 + index1, test, &str_tab1);

    /*Il reste a écrire */
    /* le header de la table des symboles dans la table header_resultat en modifiant le sh_link*/
    /* le header de la stringtable dans la table_header resultat */
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    fclose(obj1);
    fclose(obj2);

    free(header1);
    free(header2);

    free(sh_table1);
    free(sh_table2);
    free(sh_table_res);
    free(assoc_name);

    for ( i = 0 ; i < sh_table1_size ; i++ ) {
        free(sh_table1_names[i]);
    }

    free(sh_table1_names);

    for ( i = 0 ; i < sh_table2_size ; i++ ) {
        free(sh_table2_names[i]);
    }

    free(sh_table2_names);

    for ( i = 0 ; i < sh_table_res_size ; i++ ) {
        free(table_section[i]);
    }

    free(table_section);

    free(section_offset);
    free(sh_table1_ndx);
    free(sh_table2_ndx);

    return 0;
}

