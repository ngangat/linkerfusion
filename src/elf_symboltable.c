#include "elf_symboltable.h"


Elf32_Sym Elf32_read_Esym(FILE *file)
{
    Elf32_Sym sym;
    fread(&sym.st_name, sizeof(Elf32_Word), 1, file);
    fread(&sym.st_value, sizeof(Elf32_Addr), 1, file);
    fread(&sym.st_size, sizeof(Elf32_Word), 1, file);
    fread(&sym.st_info, sizeof(unsigned char), 1, file);
    fread(&sym.st_other, sizeof(unsigned char), 1, file);
    fread(&sym.st_shndx, sizeof(Elf32_Half), 1, file);
    return sym;
}

int Elf32_read_symtb(FILE *file, Elf32_Shdr *symtab_hdr, Elf32_Sym **symtb)
{
    int nb = symtab_hdr->sh_size / symtab_hdr->sh_entsize;
    int chck = 0;

    chck = fseek(file, symtab_hdr->sh_offset, SEEK_SET);

    if (chck == -1) {
        return -1;
    }

    else {
        *symtb = malloc(sizeof(Elf32_Sym) * nb);

        if (symtb == NULL) {
            return -2;
        } else {
            int i;

            for (i = 0; i < nb; i++) {
                (*symtb)[i] = Elf32_read_Esym(file);
            }

            return nb;
        }
    }
}

void Elf32_print_sym(Elf32_Sym *sym, char **str_tab)
{
    int bind, type;
    char *b;
    char *t;

    bind = ELF32_ST_BIND(sym->st_info);
    type = ELF32_ST_TYPE(sym->st_info);

    if (bind == 0) {
        b = "STB_LOCAL";
    } else if (bind == 1) {
        b = "STB_GLOBAL";
    } else if (bind == 2) {
        b = "STB_WEAK";
    } else if (bind == 13) {
        b = "STB_LOPROC";
    } else if (bind == 15) {
        b = "STB_HIPROC";
    } else {
        b = "INVALID";
    }

    if (type == 0) {
        t = "STT_NOTYPE";
    } else if (type == 1) {
        t = "STT_OBJET";
    } else if (type == 2) {
        t = "STT_FUNC";
    } else if (type == 3) {
        t = "STT_SECTION";
    } else if (type == 4) {
        t = "STT_FILE";
    } else if (type == 13) {
        t = "STT_LOPROC";
    } else if (type == 15) {
        t = "STT_HIPROC";
    } else {
        t = "INVALID";
    }

    printf("%10s\t%d\t%d\t%s\t%s\t%d\t%x \n",
           (*str_tab)+sym->st_name, sym->st_value, sym->st_size, b, t, sym->st_other,
           sym->st_shndx);
}

void Elf32_print_symtb(Elf32_Sym *symtb, Elf32_Shdr *sect, int nb, char **str_tab)
{
    int i;
    printf("%10s\t%s\t%s\t%s\t%s\t%s\t%s \n","name","value","size","bind","type","other","shndx");

    for (i = 0; i < nb; i++) {
        Elf32_print_sym(symtb+i, str_tab);
    }

}

int Elf32_read_symtb_strtb(FILE *file, Elf32_Shdr *sh_symtb, Elf32_Shdr **sh_table, char **str_tab)
{
    int chck = Elf32_read_sect(file, (*sh_table)+(sh_symtb->sh_link), str_tab) ;

    if (chck != 0) {
        printf("erreur au niveau du readsect\n");
        return ERRCODE_BADALLOC ;
    }

    return 0 ;
}

