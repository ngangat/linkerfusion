#include <stdio.h>
#include "types.h"
#include "elf_header.h"
#include "elf_sectiontable.h"
#include "elf_readsect.h"

int main(int argc, char **argv)
{
    int i, j;
    FILE *objfile;
    Elf32_Ehdr *header;
    Elf32_Shdr *sh_table = NULL;
    int sh_table_size;

    for (i = 1; i < argc; i++) {
        if (!(objfile = fopen(argv[i], "r"))) {
            printf("Error: %s is not a file\n", argv[i]);
            fclose(objfile);
            continue;
        }

        if (!Elf32_verify_magic(objfile)) {
            printf("Error: magic number doesn't match ELF format\n");

        }

        Elf32_read_header(objfile, &header);

        printf("--[ELF section table for %s]\n", argv[i]);

        sh_table_size = Elf32_read_sectiontable(objfile, header, &sh_table);

        if (sh_table_size != header->e_shnum) {
            printf("Error: when trying to parse section header table (errcode %d)\n",sh_table_size);
            break;
        }

        printf("N\tNameId\tType\t\tAddr.\tOffset\tSize\tEntSize\tFlags\tLink\tInfo\tAlign.\n");

        for (j = 0; j < sh_table_size; j++) {
            printf("[%d]\t", j);
            Elf32_print_sheader(&(sh_table[j]));
        }

        Elf32_printf_sect(objfile, &(sh_table[1]));
        Elf32_printf_sect(objfile, &(sh_table[2]));
        Elf32_printf_sect(objfile, &(sh_table[8]));

        printf("--\n");

        fclose(objfile);
        free(sh_table);
    }

    return 0;
}
