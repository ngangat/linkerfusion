#include "elf_fusion_sym.h"


int fusion_stringtable(Elf32_Shdr *head1, char **table1, Elf32_Shdr *head2, char **table2)
{
    int i, offset;

    *table1=realloc((*table1), head1->sh_size+head2->sh_size);

    if (table1==NULL) {
        printf("échec realloc \n");
        return -1;
    }

    for ( i=0 ; i < head2->sh_size ; i++) {
        (*table1)[i+head1->sh_size] = (*table2)[i];
    }

    /* for ( i=0 ; i < head2->sh_size+head1->sh_size ; i++)
     {
    printf("boucle : %d \n", i);
    printf("boucle : %s \n", (*table1)+i);
         }*/
    offset=head1->sh_size;
    head1->sh_size=head1->sh_size+head2->sh_size;
    return offset;
}

/* met a jour les adresse des nom */
/* paramètre resultat: table1: paramètre nb (offset a appliquer)*/
/* code d'erreur éventuel  */
/* */
int fusion_modifname(int nb, Elf32_Sym **symtable, int tailletable)
{
    int i;

//     printf("nb : %d \n", nb);
    for ( i=0 ; i < tailletable ; i++) {
//          printf("boucle : %d \n", i);
//  printf("valeur avant : %d \n", ((*symtable)+i)->st_name);
        (*symtable)[i].st_name = (*symtable)[i].st_name + nb;
//  printf("valeur après : %d \n", ((*symtable)+i)->st_name);
    }

    return 0;
}


/* met a jour les adresse des symboles dans les fonctions */
/* paramètre resultat: table1 : paramètre offset*/
/* code d'erreur éventuel  */
/* */
int fusion_modif_offset(Elf32_Sym **symtable2, int tailletable, int *offset)
{
    int i;

    for(i=0; i<tailletable; i++) {
        (*symtable2)[i].st_value= (*symtable2)[i].st_value+ offset[i];
    }

    return 0;
}


/* met a jour les adresse des symboles dans les fonctions */
/* paramètre resultat: table1 : paramètre offset*/
/* code d'erreur éventuel  */
/* */
int fusion_maj_shndx(Elf32_Sym **symtable2, int tailletable, int *indice)
{
    int i;

    for(i=0; i<tailletable; i++) {
        if((*symtable2)[i].st_shndx != SHN_ABS) {
            (*symtable2)[i].st_shndx = indice[i];
        }

        /* Voir les fichiers */
    }

    return 0;
}
/* fusionne les tables des symboles */
/* paramètre resultat: table1 : paramètre offset*/
/* code d'erreur éventuel  */
/* */
int fusionner_table(Elf32_Sym **symtable1, Elf32_Sym **symtable2, Elf32_Shdr *head1, Elf32_Shdr *head2, char **table1, int taille1, int taille2)
{
    int i;
    int append=taille1;

    for(i=1; i <taille2; i++) {
        //Elf32_print_sym((*symtable2)+i, table1);
        int bind = ELF32_ST_BIND((*symtable2)[i].st_info);

        if (bind==0) {
            /* On doit append la table des symbole n°1*/
            *symtable1=realloc((*symtable1), (append+1)*sizeof(Elf32_Sym));
            (*symtable1)[append]=(*symtable2)[i];
            append=append+1;
        } else if (bind==1) { /* Si le symbole est global*/
            /*on va rechercher dans la table des symboles 1*/
            int j;
            char *name = (*table1)+(*symtable2)[i].st_name ;

            for(j=1; j <taille1; j++) {
                if (strcmp(name,(*table1)+(*symtable1)[j].st_name)==0) {
                    if (ELF32_ST_BIND((*symtable1)[j].st_info)!=1)
                        /*Si le symbole existe mais n'est pas GLOBAL, on écrit*/
                    {
                        *symtable1=realloc((*symtable1), (append+1)*sizeof(Elf32_Sym));
                        (*symtable1)[append]=(*symtable2)[i];
                        append=append+1;
                        break;
                    } else if(ELF32_ST_TYPE((*symtable2)[j].st_info)==STT_NOTYPE)
                        /*si le symbole existe mais n'est pas définit, on écrit pas,on laisse l'autre*/
                        /*Si le symbole existe et est GLOBAL, on génère une erreur qui arrète la fusion*/
                    {
                        break;
                    } else if(ELF32_ST_TYPE((*symtable1)[j].st_info)==STT_NOTYPE)
                        /*le symbole existe deja mais il faut écraser la version indéfinie*/
                    {
                        (*symtable1)[j]=(*symtable2)[i];
                        break;
                    } else
                        /*Si le symbole existe et est GLOBAL, on génère une erreur qui arrète la fusion*/
                    {
                        return -1;
                    }
                }
            }

            if (j==taille1) { /*Si le symbole n'existe pas, on écrit*/
                *symtable1=realloc((*symtable1), (append+1)*sizeof(Elf32_Sym));
                (*symtable1)[append]=(*symtable2)[i];
                append=append+1;
            }
        }
    }

    /*on met a jour le header1 pour prendre en compte la nouvelle taille de la table des symboles*/

    head1->sh_size=append*head1->sh_entsize;

    return append;
}




















