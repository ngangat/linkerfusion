#include "elf_header.h"

#define EN_INVALID "invalid"

#define EN_ELFCLASS32 "32-bit"
#define EN_ELFCLASS64 "64-bit"

#define EN_ELFDATA2LSB "little endian"
#define EN_ELFDATA2MSB "big endian"

#define EN_ET_NONE "no file type"
#define EN_ET_REL "relocatable"
#define EN_ET_EXEC "executable"
#define EN_ET_DYN "shared object file"
#define EN_ET_CORE "core file"
#define EN_ET_LOPROC "processor-specific (ET_LOPROC=0xff00)"
#define EN_ET_HIPROC "processor-specific (ET_HIPROC=0xffff)"

#define EN_EM_NONE "no machine or unknown"
#define EN_EM_M32 "AT&T WE 32100"
#define EN_EM_SPARC "SPARC"
#define EN_EM_386 "Intel 80386"
#define EN_EM_68K "Motorola 68000"
#define EN_EM_88K "Motorola 88000"
#define EN_EM_860 "Intel 80860"
#define EN_EM_MIPS "MPIS RS3000"

int Elf32_read_header(FILE *file, Elf32_Ehdr **header)
{
    unsigned int i;
    Elf32_Ehdr *ptr_header;

    *header = malloc(sizeof(Elf32_Ehdr));
    ptr_header = *header;

    if (ptr_header == NULL) {
        return ERRCODE_BADALLOC;
    }

    fseek(file, 0, SEEK_SET);

    for (i = 0; i < EI_NIDENT; i++) {
        ptr_header->e_ident[i] = fgetc(file);
    }

    fread(&ptr_header->e_type, sizeof(Elf32_Half), 1, file);
    fread(&ptr_header->e_machine, sizeof(Elf32_Half), 1, file);
    fread(&ptr_header->e_version, sizeof(Elf32_Word), 1, file);
    fread(&ptr_header->e_entry, sizeof(Elf32_Addr), 1, file);
    fread(&ptr_header->e_phoff, sizeof(Elf32_Off), 1, file);
    fread(&ptr_header->e_shoff, sizeof(Elf32_Off), 1, file);
    fread(&ptr_header->e_flags, sizeof(Elf32_Word), 1, file);
    fread(&ptr_header->e_ehsize, sizeof(Elf32_Half), 1, file);
    fread(&ptr_header->e_phentsize, sizeof(Elf32_Half), 1, file);
    fread(&ptr_header->e_phnum, sizeof(Elf32_Half), 1, file);
    fread(&ptr_header->e_shentsize, sizeof(Elf32_Half), 1, file);
    fread(&ptr_header->e_shnum, sizeof(Elf32_Half), 1, file);

    if (!fread(&ptr_header->e_shstrndx, sizeof(Elf32_Half), 1, file)) {
        free(ptr_header);
        *header = NULL;
        return ERRCODE_READFILE;
    }

    return 0;
}

int Elf32_verify_magic(FILE *file)
{
    char byte0, byte1, byte2, byte3;

    byte0 = fgetc(file);
    byte1 = fgetc(file);
    byte2 = fgetc(file);
    byte3 = fgetc(file);

    return byte0 == ELFMAG0
           && byte1 == ELFMAG1
           && byte2 == ELFMAG2
           && byte3 == ELFMAG3;
}

void Elf32_print_header(Elf32_Ehdr *header)
{
    char *class;
    char *data;
    char *type;
    char *archi;

    if (header->e_ident[EI_CLASS] == ELFCLASS32) {
        class = EN_ELFCLASS32;
    } else if (header->e_ident[EI_CLASS] == ELFCLASS64) {
        class = EN_ELFCLASS64;
    } else {
        class = EN_INVALID;
    }

    if (header->e_ident[EI_DATA] == ELFDATA2LSB) {
        data = EN_ELFDATA2LSB;
    } else if (header->e_ident[EI_DATA] == ELFDATA2MSB) {
        data = EN_ELFDATA2MSB;
    } else {
        data = EN_INVALID;
    }

    if (header->e_type == ET_REL) {
        type = EN_ET_REL;
    } else if (header->e_type == ET_EXEC) {
        type = EN_ET_EXEC;
    } else if (header->e_type == ET_DYN) {
        type = EN_ET_DYN;
    } else if (header->e_type == ET_CORE) {
        type = EN_ET_CORE;
    } else if (header->e_type == ET_LOPROC) {
        type = EN_ET_LOPROC;
    } else if (header->e_type == ET_HIPROC) {
        type = EN_ET_HIPROC;
    } else {
        type = EN_ET_NONE;
    }

    if (header->e_machine == EM_M32) {
        archi = EN_EM_M32;
    } else if (header->e_machine == EM_SPARC) {
        archi = EN_EM_SPARC;
    } else if (header->e_machine == EM_386) {
        archi = EN_EM_386;
    } else if (header->e_machine == EM_68K) {
        archi = EN_EM_68K;
    } else if (header->e_machine == EM_88K) {
        archi = EN_EM_88K;
    } else if (header->e_machine == EM_860) {
        archi = EN_EM_860;
    } else if (header->e_machine == EM_MIPS) {
        archi = EN_EM_MIPS;
    } else {
        archi = EN_EM_NONE;
    }

    printf("Archi: %s\n", archi);
    printf("Class: %s\n", class);
    printf("Data: %s\n", data);
    printf("Type: %s\n", type);
    printf("Flags: 0x%x\n", header->e_flags);

    if (header->e_ident[EI_VERSION] == EV_NONE) {
        printf("ELF header version number: %s (EV_CURRENT=%d)\n", EN_INVALID, EV_CURRENT);
    } else {
        printf("ELF header version number: %d\n", header->e_ident[EI_VERSION]);
    }

    if (header->e_version == EV_NONE) {
        printf("Object file version: %s (EV_CURRENT=%d)\n", EN_INVALID, EV_CURRENT);
    } else {
        printf("Object file version: %d\n", header->e_version);
    }

    printf("ELF header's size: %d\n", header->e_ehsize);

    printf("Program header table's file offset: %d\n", header->e_phoff);
    printf("Program header entry size: %d\n", header->e_phentsize);
    printf("Number of entries in program header table: %d\n", header->e_phnum);

    printf("Section header table's file offset: %d\n", header->e_shoff);
    printf("Section header entry size: %d\n", header->e_shentsize);
    printf("Number of entries in section header table: %d\n", header->e_shnum);

    printf("Index of the entry of the string table: 0x%x\n", header->e_shstrndx);
}
