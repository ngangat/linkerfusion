#include <stdio.h>
#include <stdlib.h>
#include "elf_header.h"
#include "types.h"
#include "elf_sectiontable.h"
#include "elf_readsect.h"
#include "elf_symboltable.h"
#include "elf_relocationtable.h"
int main(int argc, char **argv)
{
    int i,j, ret;
    FILE *objfile;
    Elf32_Ehdr *header;
    Elf32_Shdr *sh_table = NULL;
    int sh_table_size;
    char *str_tab;
    int index;
    int test;
    int nb;
    Elf32_Sym *sym_table = NULL;
    Elf32_Shdr *sh_reltable = NULL;
    Elf32_Rel *rel = NULL;

    for (i = 1; i < argc; i++) {
        fprintf(stderr,"-- Reading file \"%s\" --\n",argv[i]);

        if (!(objfile = fopen(argv[i], "r"))) {
            fprintf(stderr,"Error: %s is not a file\n", argv[i]);
            continue;
        }

        if (!Elf32_verify_magic(objfile)) {
            printf("Error: magic number doesn't match ELF format for %s\n",argv[i]);
            fclose(objfile);
            continue;
        }

        ret = Elf32_read_header(objfile, &header);

        if (ret == ERRCODE_BADALLOC) {
            printf("Error: allocation failure for %s's ELF header\n",argv[i]);
            fclose(objfile);
            free(header);
            continue;
        } else if (ret == ERRCODE_READFILE) {
            printf("Error: when reading %s's ELF header\n", argv[i]);
            fclose(objfile);
            free(header);
            continue;
        }

        printf("--[ELF header for %s]\n", argv[i]);
        Elf32_print_header(header);

        printf("--[ELF section table for %s]\n", argv[i]);
        sh_table_size = Elf32_read_sectiontable(objfile, header, &sh_table);

        if (sh_table_size != header->e_shnum) {
            printf("Error: when trying to parse section header table (errcode %d)\n", sh_table_size);
            free(header);
            free(sh_table);
            fclose(objfile);
            continue;
        }

        printf("N\tNameId\tType\t\tAddr.\tOffset\tSize\tEntSize\tFlags\tLink\tInfo\tAlign.\n");

        for (j = 0; j < sh_table_size; j++) {
            printf("[%d]\t", j);
            Elf32_print_sheader(&(sh_table[j]));
        }

        printf("\n--[Sections details]--\n");

        Elf32_printf_sect(objfile, &(sh_table[1]));
        Elf32_printf_sect(objfile, &(sh_table[2]));
        Elf32_printf_sect(objfile, &(sh_table[8]));

        printf("\n--[Reading symbols details]--\n");
        index = Elf32_seek_indexsymtab(header, sh_table);
        test= Elf32_read_symtb_strtb(objfile, sh_table+index, &sh_table, &str_tab);

        if( test == ERRCODE_BADALLOC ) {
            printf("Error: allocation failure for %s's ELF string table\n", argv[i]);
            continue;
        }

        if( test == ERRCODE_READFILE ) {
            printf("Error: reading failure for %s's ELF string table\n", argv[i]);
            continue;
        }

        nb = Elf32_read_symtb(objfile, sh_table + index, &sym_table);

        if( nb == ERRCODE_READFILE ) {
            printf("Error: reading failure for %s's ELF sym table\n", argv[i]);
            continue;
        }

        if( nb == ERRCODE_BADALLOC ) {
            printf("Error: allocation failure for %s's ELF sym table\n", argv[i]);
            continue;
        }

        Elf32_print_symtb(sym_table, sh_table+index, nb, &str_tab);

        printf("\n--[Reading Relocation tables]--\n");
        sh_table_size = Elf32_read_relocationtable(header, sh_table, &sh_reltable);

        if( sh_table_size == ERRCODE_BADALLOC ) {
            printf("Error: allocation failure for %s's ELF string table\n", argv[i]);
            fclose(objfile);
            free(header);
            free(sh_table);
            continue;
        }

        for (j = 0; j < sh_table_size; j++) {
            ret = Elf32_seek_rel(objfile, sh_reltable + j, &rel);

            if (ret < 0) {
                printf("Error: when trying to parse relocation table (errcode %d)\n", ret);
                continue;
            }

            printf("Relocation table [%d]\n", j);
            printf("relocation offset : %u\n", rel->r_offset);
            printf("relocation info : %x\n\n", rel->r_info);
        }



        fclose(objfile);
        free(header);
        free(sh_reltable);
        free(sh_table);
    }

    return 0;
}
