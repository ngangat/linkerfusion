#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "elf_header.h"
#include "elf_sectiontable.h"
#include "elf_fusionrenumsect.h"

int main(int argc, char **argv)
{
    FILE *obj1;
    FILE *obj2;
    /*FILE* obj_out;*/

    Elf32_Ehdr *header1;
    Elf32_Ehdr *header2;

    Elf32_Shdr *sh_table1;
    Elf32_Shdr *sh_table2;
    int sh_table1_size, sh_table2_size;

    char **sh_table1_names;
    char **sh_table2_names;

    Elf32_Shdr *sh_table_res;
    int sh_table_res_size;
    char **assoc_name;

    char **table_section;

    int *section_offset;
    int *sh_table1_ndx;
    int *sh_table2_ndx;

    int i, j, ret;

    Elf32_Half sh_table_num1 = header1->e_shnum;
    int nb_rel1 = 0;
    Elf32_Shdr *sh_reltable1;

    Elf32_Half sh_table_num2 = header2->e_shnum;
    int nb_rel2 = 0;
    Elf32_Shdr *sh_reltable2;


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    if ( argc != 4 ) {
        printf("Error: syntax is\n> %s obj_1 obj_2 obj_out\n", argv[0]);
        return -1;
    }

    if (!(obj1 = fopen(argv[1], "r"))) {
        printf("Error: cannot open %s with mode \"w\"\n", argv[1]);
        return -1;
    }

    if (!(obj2 = fopen(argv[2], "r"))) {
        printf("Error: cannot open %s with mode \"w\"\n", argv[2]);
        return -1;
    }

    /*
    if (!(obj_out = fopen(argv[3], "w"))) {
      printf("Error: cannot open %s with mode \"r\"\n", argv[3]);
      return ;
    }
     */


    if ( !Elf32_verify_magic(obj1) ) {
        printf("Error: magic number doesn't match ELF format for %s\n", argv[1]);
        fclose(obj1);
        return -1;
    }

    if ( !Elf32_verify_magic(obj2) ) {
        printf("Error: magic number doesn't match ELF format for %s\n", argv[2]);
        fclose(obj2);
        return -1;
    }


    ret = Elf32_read_header(obj1, &header1);

    if ( ret == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF header\n", argv[1]);
        fclose(obj1);
        free(header1);
        return -1;
    } else if ( ret == ERRCODE_READFILE ) {
        printf("Error: when reading %s's ELF header\n", argv[1]);
        fclose(obj1);
        free(header1);
        return -1;
    }

    ret = Elf32_read_header(obj2, &header2);

    if ( ret == ERRCODE_BADALLOC ) {
        printf("Error: allocation failure for %s's ELF header\n", argv[2]);
        fclose(obj2);
        free(header2);
        return -1;
    } else if ( ret == ERRCODE_READFILE ) {
        printf("Error: when reading %s's ELF header\n", argv[2]);
        fclose(obj2);
        free(header2);
        return -1;
    }

    sh_table1_size = Elf32_read_sectiontable(obj1, header1, &sh_table1);
    sh_table2_size = Elf32_read_sectiontable(obj2, header2, &sh_table2);

    sh_table1_names = malloc(sizeof(char *) * sh_table1_size);

    for ( i = 0 ; i < sh_table1_size ; i++ ) {
        sh_table1_names[i] = Elf32_seek_nameshstr(obj1, Elf32_seek_shstr(header1, sh_table1), &sh_table1[i]);
    }

    sh_table2_names = malloc(sizeof(char *) * sh_table2_size);

    for ( i = 0 ; i < sh_table2_size ; i++ ) {
        sh_table2_names[i] = Elf32_seek_nameshstr(obj2, Elf32_seek_shstr(header2, sh_table2), &sh_table2[i]);
    }




    sh_table_res_size = Elf32_fusion_progbits(obj1, header1, sh_table1, sh_table1_names
                                              , obj2, header2, sh_table2, sh_table2_names
                                              , &sh_table_res, &assoc_name, &table_section, &section_offset
                                              , &sh_table1_ndx, &sh_table2_ndx
                                             );

    printf("FusOff\t| N\tName\t\tNamenxd\tType\t\tAddr.\tOffset\tSize\tEntSize\tFlags\tLink\tInfo\tAlign.\n");
    printf("\t| [0]\tNULL\n");

    for ( i = 1 ; i < sh_table_res_size ; i++ ) {
        printf("%d\t| [%d]\t", section_offset[i], i);

        printf("%s", assoc_name[i]);

        if ( strlen(assoc_name[i]) >= 8 ) {
            printf("\t");
        } else {
            printf("\t\t");
        }

        Elf32_print_sheader(&sh_table_res[i]);
    }

    /*******************************************/
    for (i = 0; i < sh_table_num1; i++)
        if (sh_table1[i].sh_type == SHT_REL) {
            nb_rel1++;
        }

    for (i = 0; i < sh_table_num2; i++)
        if (sh_table2[i].sh_type == SHT_REL) {
            nb_rel2++;
        }

    if (nb_rel1 == 0) {
        printf("No relocation table found in first file");
    } else {
        sh_reltable1 = malloc(sizeof(Elf32_Shdr) * nb_rel1);

        if (sh_reltable1 == NULL) {
            printf(" Error : Bad alloc");
        }

        nb_rel1 = 0;

        for (i = 0; i < sh_table_num1; i++) {
            if (sh_table1[i].sh_type == SHT_REL) {
                sh_reltable1[nb_rel1] = sh_table1[i];
                nb_rel1++;
            }
        }

    }

    if (nb_rel2 == 0) {
        printf("No relocation table found in second file.");
    } else {
        sh_reltable2 = malloc(sizeof(Elf32_Shdr) * nb_rel2);

        if (sh_reltable2 == NULL) {
            printf(" Error : Bad alloc");
        }

        nb_rel2 = 0;

        for (i = 0; i < sh_table_num2; i++) {
            if (sh_table2[i].sh_type == SHT_REL) {
                sh_reltable2[nb_rel2] = sh_table2[i];
                nb_rel2++;
            }
        }
    }

    for(i = 0; i < nb_rel1; i++) {
        for(j = 0; j < nb_rel2; j++) {

        }
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    fclose(obj1);
    fclose(obj2);

    free(header1);
    free(header2);

    free(sh_table1);
    free(sh_table2);
    free(sh_table_res);
    free(assoc_name);

    for ( i = 0 ; i < sh_table1_size ; i++ ) {
        free(sh_table1_names[i]);
    }

    free(sh_table1_names);

    for ( i = 0 ; i < sh_table2_size ; i++ ) {
        free(sh_table2_names[i]);
    }

    free(sh_table2_names);

    for ( i = 0 ; i < sh_table_res_size ; i++ ) {
        free(table_section[i]);
    }

    free(table_section);

    free(section_offset);
    free(sh_table1_ndx);
    free(sh_table2_ndx);

    return 0;
}

