#include <stdio.h>
#include <stdlib.h>
#include "elf_header.h"
#include "types.h"

int main(int argc, char **argv)
{
    int i, ret;
    FILE *objfile;
    Elf32_Ehdr *header;

    for (i = 1; i < argc; i++) {
        if (!(objfile = fopen(argv[i], "r"))) {
            printf("Error: %s is not a file\n", argv[i]);
            continue;
        }

        if (!Elf32_verify_magic(objfile)) {
            printf("Error: magic number doesn't match ELF format for %s\n",argv[i]);
            fclose(objfile);
            continue;
        }

        ret = Elf32_read_header(objfile, &header);

        if (ret == ERRCODE_BADALLOC) {
            printf("Error: allocation failure for %s's ELF header\n",argv[i]);
            fclose(objfile);
            free(header);
            continue;
        } else if (ret == ERRCODE_READFILE) {
            printf("Error: when reading %s's ELF header\n", argv[i]);
            fclose(objfile);
            free(header);
            continue;
        }

        printf("--[ELF header for %s]\n", argv[i]);
        Elf32_print_header(header);

        fclose(objfile);
        free(header);
        header = NULL;
    }

    return 0;
}
