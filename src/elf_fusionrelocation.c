#include "elf_fusionrelocation.h"

char *Elf32_shconcat(FILE *obj1, Elf32_Shdr *sh_obj1, FILE *obj2, Elf32_Shdr *sh_obj2)
{
    unsigned int i;
    char *sect1;
    char *sect2;
    char *sect_res;

    Elf32_read_sect(obj1, sh_obj1, &sect1);
    Elf32_read_sect(obj2, sh_obj2, &sect2);

    sect_res = malloc(sizeof(char) * (sh_obj1->sh_size + sh_obj2->sh_size + 1));

    for ( i = 0 ; i < sh_obj2->sh_size ; i++ ) {
        sect_res[i] = sect2[i];
    }

    for ( i = 0 ; i < sh_obj1->sh_size ; i++ ) {
        sect_res[sh_obj2->sh_size + i] = sect1[i];
    }

    sect_res[sh_obj1->sh_size + sh_obj2->sh_size] = '\0';

    free(sect1);
    free(sect2);

    return sect_res ;
}

int Elf32_fusion_rel(FILE *obj1, Elf32_Ehdr *header1, Elf32_Shdr *sh_table1, char **sh_table1_names
                     , FILE *obj2, Elf32_Ehdr *header2, Elf32_Shdr *sh_table2, char **sh_table2_names
                     , Elf32_Shdr **sh_table_res, char *** assoc_name, char *** table_section, int **section_offset
                     , int **sh_table1_ndx, int **sh_table2_ndx
                    )
{
    Elf32_Word i, j, count=1;
    Elf32_Half sh_table1_size = header1->e_shnum;
    Elf32_Half sh_table2_size = header2->e_shnum;
    char *section;

    for ( i = 1 ; i < sh_table2_size ; i++ ) {
        for ( j = 1 ; j < sh_table1_size ; j++ ) {
            if ( sh_table2[i].sh_type == sh_table1[j].sh_type
                    && sh_table2[i].sh_type == SHT_REL
                    && strcmp(sh_table2_names[i], sh_table1_names[j]) == 0
               ) {
                section = Elf32_shconcat(obj1, sh_table1, objz2, sh_table2); /* memcpy */
                (*sh_table_res)[sh_table2_ndx[i]] = sh_table2[i];
                (*sh_table_res)[sh_table1_ndx[i]].sh_flags |= sh_table1[j].sh_flags; /* à vérifier */
                (*sh_table_res)[sh_table1_ndx[i]].sh_size += sh_table1[j].sh_size;

                (*table_section)[i] = section;
                (*section_offset)[i] = sh_table1[j].sh_size;

                (*sh_table2_ndx)[i] = i;

                count++;
                break;
            }
        }

        if ( !(sh_table2[i].sh_type == sh_table1[j].sh_type
                && sh_table2[i].sh_type == SHT_PROGBITS
                && strcmp(sh_table2_names[i], sh_table1_names[j]) == 0
              )) {
            (*sh_table_res)[i] = sh_table2[i];

            (*assoc_name)[i] = sh_table2_names[i];

            Elf32_read_sect(obj2, &sh_table2[i], &section);
            (*table_section)[i] = section;
            (*section_offset)[i] = 0;
            (*sh_table2_ndx)[i] = i;
            count++;
        }
    }

    for ( i = 1 ; i < sh_table1_size ; i++ ) {
        for ( j = 1 ; j < sh_table2_size ; j++ ) {
            if ( sh_table1[i].sh_type == sh_table2[j].sh_type
                    && sh_table1[i].sh_type == SHT_PROGBITS
                    && strcmp(sh_table1_names[i], sh_table2_names[j]) == 0
               ) {
                break;
            }
        }

        if ( j == sh_table2_size ) {
            (*sh_table_res)[count] = sh_table1[i];

            (*assoc_name)[count] = sh_table1_names[i];

            Elf32_read_sect(obj1, &sh_table1[i], &section);
            (*table_section)[count] = section;
            (*section_offset)[count] = 0;
            (*sh_table1_ndx)[i] = i;

            count++;
        }

    }

    *sh_table_res = realloc(*sh_table_res, sizeof(Elf32_Shdr) * count);
    return count;
}
