#include <stdio.h>
#include "types.h"
#include "elf_header.h"
#include "elf_sectiontable.h"
#include "string.h"

int main(int argc, char **argv)
{
    int i, j, ret, sh_table_size;
    FILE *objfile;
    Elf32_Ehdr *header;
    Elf32_Shdr *sh_table = NULL;
    char *sname;

    for (i = 1; i < argc; i++) {
        if (!(objfile = fopen(argv[i], "r"))) {
            printf("Error: %s is not a file\n", argv[i]);
            continue;
        }

        if ( !Elf32_verify_magic(objfile) ) {
            printf("Error: magic number doesn't match ELF format for %s\n", argv[i]);
            fclose(objfile);
            continue;
        }

        ret = Elf32_read_header(objfile, &header);

        if ( ret == ERRCODE_BADALLOC ) {
            printf("Error: allocation failure for %s's ELF header\n", argv[i]);
            fclose(objfile);
            free(header);
            continue;
        } else if ( ret == ERRCODE_READFILE ) {
            printf("Error: when reading %s's ELF header\n", argv[i]);
            fclose(objfile);
            free(header);
            continue;
        }

        printf("--[ELF section table for %s]\n", argv[i]);
        sh_table_size = Elf32_read_sectiontable(objfile, header, &sh_table);

        if (sh_table_size != header->e_shnum) {
            printf("Error: when trying to parse section header table (errcode %d)\n", sh_table_size);
            free(header);
            free(sh_table);
            fclose(objfile);
            continue;
        }

        printf("N\tName\t\tNamenxd\tType\t\tAddr.\tOffset\tSize\tEntSize\tFlags\tLink\tInfo\tAlign.\n");

        for (j = 0; j < sh_table_size; j++) {
            sname = Elf32_seek_nameshstr(objfile, Elf32_seek_shstr(header, sh_table), &sh_table[j]);
            printf("[%d]\t", j);
            printf("%s", sname);

            if ( strlen(sname) >= 8 ) {
                printf("\t");
            } else {
                printf("\t\t");
            }

            Elf32_print_sheader(&sh_table[j]);
            free(sname);
        }

        fclose(objfile);
        free(header);
        free(sh_table);
    }

    return 0;
}
