#include "elf_relocationtable.h"

#define EN_R_ARM_NONE                   "R_ARM_NONE"
#define EN_R_ARM_PC24                   "R_ARM_PC24"
#define EN_R_ARM_ABS32                  "R_ARM_ABS32"
#define EN_R_ARM_REL32                  "R_ARM_REL32"
#define EN_R_ARM_PC13                   "R_ARM_PC13"
#define EN_R_ARM_ABS16                  "R_ARM_ABS16"
#define EN_R_ARM_ABS12                  "R_ARM_ABS12"
#define EN_R_ARM_THM_ABS5               "R_ARM_THM_ABS5"
#define EN_R_ARM_ABS8                   "R_ARM_ABS8"
#define EN_R_ARM_SBREL32                "R_ARM_SBREL32"
#define EN_R_ARM_THM_PC22               "R_ARM_THM_PC22"
#define EN_R_ARM_THM_PC8                "R_ARM_THM_PC8"
#define EN_R_ARM_AMP_VCALL9             "R_ARM_AMP_VCALL9"
#define EN_R_ARM_SWI24                  "R_ARM_SWI24"
#define EN_R_ARM_TLS_DESC               "R_ARM_TLS_DESC"
#define EN_R_ARM_THM_SWI8               "R_ARM_THM_SWI8"
#define EN_R_ARM_XPC25                  "R_ARM_XPC25"
#define EN_R_ARM_THM_XPC22              "R_ARM_THM_XPC22"
#define EN_R_ARM_TLS_DTPMOD32           "R_ARM_TLS_DTPMOD32"
#define EN_R_ARM_TLS_DTPOFF32           "R_ARM_TLS_DTPOFF32"
#define EN_R_ARM_TLS_TPOFF32            "R_ARM_TLS_TPOFF32"
#define EN_R_ARM_COPY                   "R_ARM_COPY"
#define EN_R_ARM_GLOB_DAT               "R_ARM_GLOB_DAT"
#define EN_R_ARM_JUMP_SLOT              "R_ARM_JUMP_SLOT"
#define EN_R_ARM_RELATIVE               "R_ARM_RELATIVE"
#define EN_R_ARM_GOTOFF                 "R_ARM_GOTOFF"
#define EN_R_ARM_GOTPC                  "R_ARM_GOTPC"
#define EN_R_ARM_GOT32                  "R_ARM_GOT32"
#define EN_R_ARM_PLT32                  "R_ARM_PLT32"
#define EN_R_ARM_ALU_PCREL_7_0          "R_ARM_ALU_PCREL_7_0"
#define EN_R_ARM_ALU_PCREL_15_8         "R_ARM_ALU_PCREL_15_8"
#define EN_R_ARM_ALU_PCREL_23_15        "R_ARM_ALU_PCREL_23_15"
#define EN_R_ARM_LDR_SBREL_11_0         "R_ARM_LDR_SBREL_11_0"
#define EN_R_ARM_ALU_SBREL_19_12        "R_ARM_ALU_SBREL_19_12"
#define EN_R_ARM_ALU_SBREL_27_20        "R_ARM_ALU_SBREL_27_20"
#define EN_R_ARM_TLS_GOTDESC            "R_ARM_TLS_GOTDESC"
#define EN_R_ARM_TLS_CALL               "R_ARM_TLS_CALL"
#define EN_R_ARM_TLS_DESCSEQ            "R_ARM_TLS_DESCSEQ"
#define EN_R_ARM_THM_TLS_CALL           "R_ARM_THM_TLS_CALL"
#define EN_R_ARM_GNU_VTENTRY            "R_ARM_GNU_VTENTRY"
#define EN_R_ARM_GNU_VTINHERIT          "R_ARM_GNU_VTINHERIT"
#define EN_R_ARM_THM_PC11               "R_ARM_THM_PC11"
#define EN_R_ARM_THM_PC9                "R_ARM_THM_PC9"
#define EN_R_ARM_TLS_GD32               "R_ARM_TLS_GD32"
#define EN_R_ARM_TLS_LDM32              "R_ARM_TLS_LDM32"
#define EN_R_ARM_TLS_LDO32              "R_ARM_TLS_LDO32"
#define EN_R_ARM_TLS_IE32               "R_ARM_TLS_IE32"
#define EN_R_ARM_TLS_LE32               "R_ARM_TLS_LE32"
#define EN_R_ARM_THM_TLS_DESCSEQ        "R_ARM_THM_TLS_DESCSEQ"
#define EN_R_ARM_IRELATIVE              "R_ARM_IRELATIVE"
#define EN_R_ARM_RXPC25                 "R_ARM_RXPC25"
#define EN_R_ARM_RSBREL32               "R_ARM_RSBREL32"
#define EN_R_ARM_THM_RPC22              "R_ARM_THM_RPC22"
#define EN_R_ARM_RREL32                 "R_ARM_RREL32"
#define EN_R_ARM_RABS22                 "R_ARM_RABS22"
#define EN_R_ARM_RPC24                  "R_ARM_RPC24"
#define EN_R_ARM_RBASE                  "R_ARM_RBASE"
#define EN_R_ARM_NUM                    "R_ARM_NUM"

int Elf32_read_relocationtable(Elf32_Ehdr *header,
                               Elf32_Shdr *sh_table,
                               Elf32_Shdr **sh_reltable)
{
    Elf32_Half sh_table_num = header->e_shnum;
    int i, count = 0;

    for (i = 0; i < sh_table_num; i++)
        if (sh_table[i].sh_type == SHT_REL) {
            count++;
        }

    if (count == 0) {
        return 0;
    }

    *sh_reltable = malloc(sizeof(Elf32_Shdr) * count);

    if (*sh_reltable == NULL) {
        return ERRCODE_BADALLOC;
    }

    count = 0;

    for (i = 0; i < sh_table_num; i++) {
        if (sh_table[i].sh_type == SHT_REL) {
            (*sh_reltable)[count] = sh_table[i];
            count++;
        }
    }

    return count;
}

int Elf32_seek_rel(FILE *file, Elf32_Shdr *sh_relheader, Elf32_Rel **rel)
{
    Elf32_Off offset = sh_relheader->sh_offset;
    Elf32_Word nb_rel = sh_relheader->sh_size / sh_relheader->sh_entsize;
    unsigned int i;

    *rel = malloc(sizeof(Elf32_Rel) * nb_rel);

    if (*rel == NULL) {
        return ERRCODE_BADALLOC;
    }

    if (fseek(file, offset, SEEK_SET) != 0) {
        free(*rel);
        return ERRCODE_BADMOVE;
    }

    for ( i = 0 ; i < nb_rel ; i++ ) {
        fread(&((*rel)[i].r_offset), sizeof(Elf32_Addr), 1, file);
        fread(&((*rel)[i].r_info), sizeof(Elf32_Addr), 1, file);

    }

    return nb_rel;
}
char *rtype_str(unsigned int type)
{

    switch(type) {
    case( R_ARM_NONE):
        return EN_R_ARM_NONE;
    case( R_ARM_PC24):
        return EN_R_ARM_PC24;
    case( R_ARM_ABS32):
        return EN_R_ARM_ABS32;

    case( R_ARM_REL32):
        return EN_R_ARM_REL32;

    case( R_ARM_PC13):
        return EN_R_ARM_PC13;

    case( R_ARM_ABS16):
        return EN_R_ARM_ABS16;

    case( R_ARM_ABS12):
        return EN_R_ARM_ABS12;

    case( R_ARM_THM_ABS5):
        return EN_R_ARM_THM_ABS5;

    case( R_ARM_ABS8):
        return EN_R_ARM_ABS8;

    case( R_ARM_SBREL32):
        return EN_R_ARM_SBREL32;

    case( R_ARM_THM_PC22):
        return EN_R_ARM_THM_PC22;

    case( R_ARM_THM_PC8):
        return EN_R_ARM_THM_PC8;

    case( R_ARM_AMP_VCALL9):
        return EN_R_ARM_AMP_VCALL9;

    case( R_ARM_TLS_DESC):
        return EN_R_ARM_TLS_DESC;

    case( R_ARM_THM_SWI8):
        return EN_R_ARM_THM_SWI8;

    case( R_ARM_XPC25):
        return EN_R_ARM_XPC25;

    case( R_ARM_THM_XPC22):
        return EN_R_ARM_THM_XPC22;

    case( R_ARM_TLS_DTPMOD32):
        return EN_R_ARM_TLS_DTPMOD32;

    case( R_ARM_TLS_DTPOFF32):
        return EN_R_ARM_TLS_DTPOFF32;

    case( R_ARM_TLS_TPOFF32):
        return EN_R_ARM_TLS_TPOFF32;

    case( R_ARM_COPY):
        return EN_R_ARM_COPY;

    case( R_ARM_GLOB_DAT):
        return EN_R_ARM_GLOB_DAT;

    case( R_ARM_JUMP_SLOT):
        return EN_R_ARM_JUMP_SLOT;

    case( R_ARM_RELATIVE):
        return EN_R_ARM_RELATIVE;

    case( R_ARM_GOTOFF):
        return EN_R_ARM_GOTOFF;

    case( R_ARM_GOTPC):
        return EN_R_ARM_GOTPC;

    case( R_ARM_GOT32):
        return EN_R_ARM_GOT32;

    case( R_ARM_PLT32):
        return EN_R_ARM_PLT32;

    case( R_ARM_ALU_PCREL_7_0):
        return EN_R_ARM_ALU_PCREL_7_0;

    case( R_ARM_ALU_PCREL_15_8):
        return EN_R_ARM_ALU_PCREL_15_8;

    case( R_ARM_ALU_PCREL_23_15):
        return EN_R_ARM_ALU_PCREL_23_15;

    case( R_ARM_LDR_SBREL_11_0):
        return EN_R_ARM_LDR_SBREL_11_0;

    case( R_ARM_ALU_SBREL_19_12):
        return EN_R_ARM_ALU_SBREL_19_12;

    case( R_ARM_ALU_SBREL_27_20):
        return EN_R_ARM_ALU_SBREL_27_20;

    case( R_ARM_TLS_GOTDESC):
        return EN_R_ARM_TLS_GOTDESC;

    case( R_ARM_TLS_CALL):
        return EN_R_ARM_TLS_CALL;

    case( R_ARM_TLS_DESCSEQ):
        return EN_R_ARM_TLS_DESCSEQ;

    case( R_ARM_THM_TLS_CALL):
        return EN_R_ARM_THM_TLS_CALL;

    case( R_ARM_GNU_VTENTRY):
        return EN_R_ARM_GNU_VTENTRY;

    case( R_ARM_GNU_VTINHERIT):
        return EN_R_ARM_GNU_VTINHERIT;

    case( R_ARM_THM_PC11):
        return EN_R_ARM_THM_PC11;

    case( R_ARM_THM_PC9):
        return EN_R_ARM_THM_PC9;

    case( R_ARM_TLS_GD32):
        return EN_R_ARM_TLS_GD32;

    case( R_ARM_TLS_LDM32):
        return EN_R_ARM_TLS_LDM32;

    case( R_ARM_TLS_LDO32):
        return EN_R_ARM_TLS_LDO32;

    case( R_ARM_TLS_IE32):
        return EN_R_ARM_TLS_IE32;

    case( R_ARM_TLS_LE32):
        return EN_R_ARM_TLS_LE32;

    case( R_ARM_THM_TLS_DESCSEQ):
        return EN_R_ARM_THM_TLS_DESCSEQ;

    case( R_ARM_IRELATIVE):
        return EN_R_ARM_IRELATIVE;

    case( R_ARM_RXPC25):
        return EN_R_ARM_RXPC25;

    case( R_ARM_RSBREL32):
        return EN_R_ARM_RSBREL32;

    case( R_ARM_THM_RPC22):
        return EN_R_ARM_THM_RPC22;

    case( R_ARM_RREL32):
        return EN_R_ARM_RREL32;

    case( R_ARM_RABS22):
        return EN_R_ARM_RABS22;

    case( R_ARM_RPC24):
        return EN_R_ARM_RPC24;

    case( R_ARM_RBASE):
        return EN_R_ARM_RBASE;

    case( R_ARM_NUM):
        return EN_R_ARM_NUM;

    default :
        return "\0";
    }
}

